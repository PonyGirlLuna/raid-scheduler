package com.lunamatthews.raidscheduler.data

data class ScheduledEvent(
	val uniqueId: String,
	val definition: EventDefinition,
	val threadName: String,
	val time: Long,
	val schedulerId: Long,
	val serverId: Long,
	val channelId: Long,
	var messageId: Long? = null,
	val spleebs: MutableList<Long> = mutableListOf(),
	val maybes: MutableList<Long> = mutableListOf(),
	val reservations: MutableList<Long> = mutableListOf(),
	var archived: Boolean = false,
	val difficulty: String = validDifficulties[0]
)
