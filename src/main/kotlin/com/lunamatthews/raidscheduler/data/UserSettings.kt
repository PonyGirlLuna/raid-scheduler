package com.lunamatthews.raidscheduler.data

data class UserSettings(
	val user: Long,
	var timeZone: String?,
	var guardianId: String?
)
