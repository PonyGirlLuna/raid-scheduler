package com.lunamatthews.raidscheduler.data

data class BannedUserList(
	val server: Long,
	val bans: MutableList<UserBan>
)
