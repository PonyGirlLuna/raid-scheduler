package com.lunamatthews.raidscheduler.data

data class Configuration(
	val serverId: Long,
	var modChannel: Long?,
	var eventChannel: Long?,
	var secondaryEventChannel: Long?,
	var roleToPing: Long?,
	var confirmEventCreationEmote: String?,
	var cancelEventCreationEmote: String?,
	var joinEventEmote: String?,
	var maybeJoinEventEmote: String?,
	var removeFromEventEmote: String?,
	var difficultyEmoteStandard: String?,
	var difficultyEmoteAdvanced: String?,
	var difficultyEmoteExpert: String?,
	var difficultyEmoteMaster: String?,
	var difficultyEmoteGrandmaster: String?,
)
