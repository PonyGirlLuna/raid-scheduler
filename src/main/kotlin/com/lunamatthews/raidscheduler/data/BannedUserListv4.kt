package com.lunamatthews.raidscheduler.data

data class BannedUserListv4(
	val server: Long,
	val bannedUsers: MutableList<Long> = mutableListOf()
)
