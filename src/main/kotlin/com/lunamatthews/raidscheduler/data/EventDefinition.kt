package com.lunamatthews.raidscheduler.data

data class EventDefinition(
	val server: Long,
	val name: String,
	val minParticipants: Int,
	val maxParticipants: Int,
	val color: String,
	val icon: String?,
	val infoLink: String?,
	val difficulties: List<String> = listOf(),
	val defaultDifficulty: String = validDifficulties[0]
)

//TODO valid difficulties as a configurable item?
val validDifficulties = listOf(
	"standard",
	"advanced",
	"expert",
	"master",
	"grandmaster"
)
