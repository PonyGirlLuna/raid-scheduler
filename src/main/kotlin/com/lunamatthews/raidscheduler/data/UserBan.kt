package com.lunamatthews.raidscheduler.data

data class UserBan(
	val modWhoBanned: Long,
	val user: Long,
	val start: Long,
	val expiration: Long,
	val note: String
)
