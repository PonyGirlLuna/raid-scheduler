package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.ScheduledEvent
import com.lunamatthews.raidscheduler.exceptions.EventDoesNotExistException
import com.lunamatthews.raidscheduler.services.EventManager
import com.lunamatthews.raidscheduler.util.getScheduledEventByThreadId
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.channel.ServerThreadChannel
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand

class PingCommand(private val eventManager: EventManager, private val scheduledEvents: JsonDB<ScheduledEvent>) : GuildSlashCommand("ping", "Pings the main team participants for an event. Can only be used inside an event thread.") {

	val log: Logger = LogManager.getLogger(PingCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 4) {
			SlashCommand.with(name, description)
				.createForServer(server)
				.join()
		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val server = event.interaction.server.get()
		val thread = event.interaction.channel.get()
		val user = event.slashCommandInteraction.user.id

		if (thread !is ServerThreadChannel) {
			event.interaction.createImmediateResponder()
				.setContent("This command can only be used inside of an event thread.")
				.setFlags(MessageFlag.EPHEMERAL)
				.respond()
			return
		}

		try {

			val scheduledEvent = getScheduledEventByThreadId(scheduledEvents, thread.id, server.id)

			val (mainTeam, _, _) = eventManager.getTeams(scheduledEvent)

			val users = mainTeam.toMutableList()
			users.remove(user)

			val raidVoiceChannel = server.voiceChannels.find { it.connectedUserIds.contains(user) }

			if (raidVoiceChannel != null) {
				users -= raidVoiceChannel.connectedUserIds
			}

			val pingMessage = users.joinToString(" ") { "<@${it}>" }

			if (pingMessage.isBlank()) {
				event.slashCommandInteraction.createImmediateResponder()
					.setContent("Nobody would be pinged!")
					.setFlags(MessageFlag.EPHEMERAL)
					.respond()
			} else {
				event.slashCommandInteraction.createImmediateResponder()
					.setContent(pingMessage)
					.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(true).build())
					.respond()
			}

		} catch (e: EventDoesNotExistException) {
			event.interaction.createImmediateResponder()
				.setContent("Could not find an event for this thread. Make sure you're using this command in an event thread. (If you are, then this is a bug and you should tell Luna.)")
				.setFlags(MessageFlag.EPHEMERAL)
				.respond()
		}

	}

}
