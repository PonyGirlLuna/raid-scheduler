package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.Configuration
import com.lunamatthews.raidscheduler.data.ScheduledEvent
import com.lunamatthews.raidscheduler.exceptions.EventDoesNotExistException
import com.lunamatthews.raidscheduler.exceptions.UserDoesNotHavePermissionException
import com.lunamatthews.raidscheduler.services.EventManager
import com.lunamatthews.raidscheduler.util.getConfigurationByServerId
import com.lunamatthews.raidscheduler.util.getScheduledEventById
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionType

class CancelEventCommand(private val eventManager: EventManager,
                         private val scheduledEventDb: JsonDB<ScheduledEvent>,
                         private val configurationDb: JsonDB<Configuration>) : GuildSlashCommand("cancel", "Cancels an event that has already been scheduled—THERE IS NO UNDO") {

	private val log: Logger = LogManager.getLogger(CancelEventCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 1) {
			SlashCommand.with(name, description, listOf(
				SlashCommandOption.create(SlashCommandOptionType.STRING, "event-id", "The unique ID of the event to cancel", true)
			))
			.createForServer(server)
			.join()
		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val server = event.interaction.server.get()
		val user = event.slashCommandInteraction.user
		val eventId = event.slashCommandInteraction.getArgumentStringValueByName("event-id").get()

		val configuration = getConfigurationByServerId(configurationDb, server.id)

		try {

			val scheduledEvent = getScheduledEventById(scheduledEventDb, eventId)

			eventManager.cancelEvent(eventId, user, server)

			if (scheduledEvent.schedulerId != user.id) {

				MessageBuilder()
					.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
					.setContent("**Moderation Action:** <@${event.slashCommandInteraction.user.id}> has cancelled event `$eventId` (${scheduledEvent.definition.name} at <t:${scheduledEvent.time}:F>).")
					.send(server.getChannelById(configuration.modChannel!!).get().asTextChannel().get())

			}

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("Cancelled event `$eventId` (${scheduledEvent.definition.name} at <t:${scheduledEvent.time}:F>).")
				.respond()

		} catch (e: UserDoesNotHavePermissionException) {
			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("You did not create this event and do not have permission to cancel events scheduled by others.")
				.respond()
		} catch (e: EventDoesNotExistException) {
			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("Could not find event with ID `$eventId`.")
				.respond()
		}


	}

}
