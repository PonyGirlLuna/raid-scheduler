package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.BannedUserList
import com.lunamatthews.raidscheduler.data.Configuration
import com.lunamatthews.raidscheduler.util.getBannedUserListByServerId
import com.lunamatthews.raidscheduler.util.getConfigurationByServerId
import com.lunamatthews.raidscheduler.util.saveBannedUserList
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionType

class UnRestrictUserCommand(private val bannedUserListDb: JsonDB<BannedUserList>,
                            private val configurationDb: JsonDB<Configuration>) : GuildSlashCommand("unrestrict", "Removes fill-only restrictions from a user.") {

	private val log: Logger = LogManager.getLogger(UnRestrictUserCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 1) {
			SlashCommand.with(name, description, listOf(
				SlashCommandOption.create(SlashCommandOptionType.USER, "user", "The user to remove restrictions from", true)
			))
				.setDefaultEnabledForPermissions(PermissionType.MODERATE_MEMBERS)
				.createForServer(server)
				.join()
		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val server = event.interaction.server.get()
		val user = event.slashCommandInteraction.getArgumentUserValueByName("user").get()

		val configuration = getConfigurationByServerId(configurationDb, server.id)
		val banList = getBannedUserListByServerId(bannedUserListDb, server.id)

		if (banList.bans.any { it.user == user.id }) {

			banList.bans.remove(banList.bans.single { it.user == user.id })
			saveBannedUserList(bannedUserListDb, banList)

			MessageBuilder()
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setContent("**Moderation Action:** <@${event.slashCommandInteraction.user.id}> has removed fill-only restrictions from <@${user.id}>.")
				.send(server.getChannelById(configuration.modChannel!!).get().asTextChannel().get())

			event.slashCommandInteraction.createImmediateResponder()
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("Fill-only restrictions removed from <@${user.id}>.")
				.respond()

		} else {

			event.slashCommandInteraction.createImmediateResponder()
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("User <@${user.id}> was already not restricted.")
				.respond()

		}

	}

}
