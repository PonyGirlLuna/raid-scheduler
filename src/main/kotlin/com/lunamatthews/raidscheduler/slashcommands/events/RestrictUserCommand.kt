package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.BannedUserList
import com.lunamatthews.raidscheduler.data.Configuration
import com.lunamatthews.raidscheduler.data.UserBan
import com.lunamatthews.raidscheduler.data.UserSettings
import com.lunamatthews.raidscheduler.util.*
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionType
import org.javacord.api.interaction.SlashCommandUpdater
import java.time.ZoneId
import java.time.ZonedDateTime

class RestrictUserCommand(private val bannedUserListDb: JsonDB<BannedUserList>,
                          private val configurationDb: JsonDB<Configuration>,
                          private val userSettingsDb: JsonDB<UserSettings>) : GuildSlashCommand("restrict", "Restricts a user to only being able to fill for future events. Use /unrestrict to undo.") {

	private val log: Logger = LogManager.getLogger(RestrictUserCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 1) {
			SlashCommand.with(name, description, listOf(
				SlashCommandOption.create(SlashCommandOptionType.USER, "user", "The user to restrict", true)
			))
				.setDefaultEnabledForPermissions(PermissionType.MODERATE_MEMBERS)
				.createForServer(server)
				.join()
		}

		if (commandVersion < 5) {

			SlashCommandUpdater(server.slashCommands.get().single { it.name == name }.id).setSlashCommandOptions(listOf(
				SlashCommandOption.create(SlashCommandOptionType.USER, "user", "The user to restrict", true),
				SlashCommandOption.create(SlashCommandOptionType.STRING, "duration", "The expiration date or duration for the restriction", true),
				SlashCommandOption.create(SlashCommandOptionType.STRING, "note", "A note about the restriction to be shown to the user", true)
			)).updateForServer(server)

		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val server = event.interaction.server.get()
		val commandUser = event.interaction.user
		val user = event.slashCommandInteraction.getArgumentUserValueByName("user").get()
		val expirationDate = event.slashCommandInteraction.getArgumentStringValueByName("duration").get()
		val note = event.slashCommandInteraction.getArgumentStringValueByName("note").get()

		val configuration = getConfigurationByServerId(configurationDb, server.id)
		val banList = getBannedUserListByServerId(bannedUserListDb, server.id)

		if (user.id == commandUser.id) {

			event.slashCommandInteraction.createImmediateResponder()
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("Why are you trying to restrict yourself?")
				.respond()

		} else if (server.hasPermission(user, PermissionType.MODERATE_MEMBERS)) {

			event.slashCommandInteraction.createImmediateResponder()
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("User <@${user.id}> is a moderator, silly. You can't restrict moderators.")
				.respond()

		} else if (!banList.bans.any { it.user == user.id }) {

			val userTimeZoneString = getUserSettingsById(userSettingsDb, commandUser.id).timeZone
			val timeZone = if (userTimeZoneString != null) {
				ZoneId.of(userTimeZoneString)
			} else {
				ZoneId.of("America/New_York")
			}

			val ban = UserBan(commandUser.id, user.id, ZonedDateTime.now().toEpochSecond(), parseDate(expirationDate, timeZone).toEpochSecond(), note)
			banList.bans += ban

			saveBannedUserList(bannedUserListDb, banList)

			log.info("User banned: $ban")

			MessageBuilder()
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setContent("**Moderation Action:** <@${commandUser.id}> has restricted <@${user.id}> to only filling for future events.\n" +
						"**Expires on:** <t:${ban.expiration}:f>\n" +
						"**Reason:** ${ban.note}")
				.send(server.getChannelById(configuration.modChannel!!).get().asTextChannel().get())

			event.slashCommandInteraction.createImmediateResponder()
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("User <@${user.id}> has been restricted to only filling for future events.")
				.respond()

		} else {

			event.slashCommandInteraction.createImmediateResponder()
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("User <@${user.id}> is already restricted.")
				.respond()

		}

	}

}
