package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.raidscheduler.exceptions.*
import com.lunamatthews.raidscheduler.services.EventManager
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionType

class ReservationCommand(private val eventManager: EventManager) : GuildSlashCommand("reservation", "Reserves a spot for someone in a raid.") {

	private val log: Logger = LogManager.getLogger(ReservationCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 2) {
			SlashCommand.with(name, description, listOf(
				SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "add", "Reserves a spot in the raid for this user", listOf(
					SlashCommandOption.create(SlashCommandOptionType.USER, "user", "The user to reserve a spot for", true),
					SlashCommandOption.create(SlashCommandOptionType.STRING, "event", "The ID of the event to add the reservation to", true)
				)),
				SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "remove", "Removes a reservation for this user", listOf(
					SlashCommandOption.create(SlashCommandOptionType.USER, "user", "The user to remove a reservation for", true),
					SlashCommandOption.create(SlashCommandOptionType.STRING, "event", "The ID of the event from which to remove the reservation", true)
				))
			))
				.createForServer(server)
				.join()
		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val server = event.interaction.server.get()
		val commandOption = event.slashCommandInteraction.options[0].name

		val user = event.slashCommandInteraction.options[0].getArgumentUserValueByName("user").get()
		val eventId = event.slashCommandInteraction.options[0].getArgumentStringValueByName("event").get()

		try {

			when (commandOption) {
				"add" -> {
					eventManager.addReservation(eventId, user, server, event.slashCommandInteraction.user)
				}
				"remove" -> {
					eventManager.removeReservation(eventId, user, server, event.slashCommandInteraction.user)
				}
			}

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setContent("Reservation made for <@${user.id}> for event `$eventId`.")
				.respond()

		} catch (e: EventDoesNotExistException) {

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("Could not find event with ID `$eventId`.")
				.respond()

		} catch (e: UserIsSchedulerException) {

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("You cannot make a reservation for yourself!")
				.respond()

		} catch (e: UserIsBannedException) {

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("This user has been forbidden from joining events and cannot have a reservation made on their behalf.")
				.respond()

		} catch (e: UserDoesNotHavePermissionException) {

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("You did not create this event and do not have permission to reserve spots for this event.")
				.respond()

		} catch (e: TooManyParticipantsException) {

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("There are already enough reservations to fill this event and more cannot be added.")
				.respond()

		}

	}

}
