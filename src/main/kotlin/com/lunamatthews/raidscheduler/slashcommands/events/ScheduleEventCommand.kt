package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.EventDefinition
import com.lunamatthews.raidscheduler.data.UserSettings
import com.lunamatthews.raidscheduler.data.validDifficulties
import com.lunamatthews.raidscheduler.exceptions.IncompleteConfigurationException
import com.lunamatthews.raidscheduler.exceptions.InvalidDateException
import com.lunamatthews.raidscheduler.exceptions.InvalidDifficultyException
import com.lunamatthews.raidscheduler.services.EventManager
import com.lunamatthews.raidscheduler.slashcommands.AutocompleteListener
import com.lunamatthews.raidscheduler.util.getUserSettingsById
import com.lunamatthews.raidscheduler.util.parseDateTime
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.*

class ScheduleEventCommand(private val eventDefinitions: JsonDB<EventDefinition>,
                           private val eventManager: EventManager,
                           private val userSettingsDb: JsonDB<UserSettings>) : GuildSlashCommand("schedule", "Schedules an event"), AutocompleteListener {

	private val log: Logger = LogManager.getLogger(ScheduleEventCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 7) {
			SlashCommand.with(name, description).createForServer(server).join()
			updateEventOptions(server)
		}

	}

	fun updateEventOptions(server: Server) {

		SlashCommandUpdater(server.slashCommands.get().single { it.name == name }.id).setSlashCommandOptions(
			listOf(
				SlashCommandOption.createStringOption("event", "The name of the event.", true, true),
				SlashCommandOption.create(SlashCommandOptionType.STRING, "time", "The time of day (Eastern) to run the event at", true),
				SlashCommandOption.create(SlashCommandOptionType.STRING, "day", "The day on which to run the event", true),
				SlashCommandOption.createStringOption("difficulty", "The difficulty of the event (standard, master, etc)", false, true),
				SlashCommandOption.create(SlashCommandOptionType.STRING, "threadName", "The title of the thread that will be created", false)
			)
		)
		.updateForServer(server)
		.join()

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val server = event.interaction.server.get()

		val commandOptions = event.slashCommandInteraction.options
		val eventName = commandOptions[0].stringValue.get()

		val userSettings = getUserSettingsById(userSettingsDb, event.interaction.user.id)

		val dateTime = parseDateTime(commandOptions[2].stringValue.get(), commandOptions[1].stringValue.get(), userSettings.timeZone)

		val eventDefinition = eventDefinitions.getAllValues<EventDefinition>().first { it.name == eventName }

		val difficulty = if (event.slashCommandInteraction.getOptionByName("difficulty").isPresent) {
			event.slashCommandInteraction.getOptionByName("difficulty").get().stringValue.get()
		} else {
			eventDefinition.defaultDifficulty
		}.lowercase()

		val threadName = if (event.slashCommandInteraction.getOptionByName("threadName").isPresent) {
			event.slashCommandInteraction.getOptionByName("threadName").get().stringValue.get()
		} else {
			null
		}

		try {
			eventManager.createEvent(event.slashCommandInteraction, server, event.slashCommandInteraction.user.id, eventName, dateTime, userSettings.timeZone, threadName, difficulty)
		} catch (e: IncompleteConfigurationException) {
			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent("**The bot is not currently configured. Please finish configuration before use.**").respond()
		} catch (e: InvalidDateException) {
			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent("**Could not create your event because:**\nDate/time is in the past.").respond()
		} catch (e: InvalidDifficultyException) {
			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent("**Could not create your event because:**\nSelected difficulty is invalid for this event.").respond()
		}

	}

	override fun handleAutocompleteInteraction(autocompleteInteraction: AutocompleteInteraction) {

		val server = autocompleteInteraction.server.get().id
		val interactionName = autocompleteInteraction.focusedOption.name
		val value = autocompleteInteraction.focusedOption.stringValue.get()

		when (interactionName) {
			"event" -> {

				val availableEvents = eventDefinitions.getAllValues<EventDefinition>().filter { it.server == server }

				autocompleteInteraction.respondWithChoices(availableEvents.filter { value.isBlank() || it.name.startsWith(value, true) }.take(25).map {
					SlashCommandOptionChoice.create(it.name, it.name)
				})

			}
			"difficulty" -> {

				val eventName = autocompleteInteraction.arguments.single { it.name == "event" }.stringValue.get()

				val event = eventDefinitions.getAllValues<EventDefinition>().singleOrNull { it.server == server && it.name == eventName }
				val difficulties = event?.difficulties?.toMutableList() ?: mutableListOf()

				if (difficulties.isEmpty()) {
					difficulties += validDifficulties[0]
				}

				val outputDifficulties = difficulties.sortedWith(compareBy<String> {
					if (it == event?.defaultDifficulty) {
						"a"
					} else {
						"b"
					}
				}.thenBy {
					it
				}).filter { it.startsWith(value) }.toMutableList()

				autocompleteInteraction.respondWithChoices(outputDifficulties.take(25).map { SlashCommandOptionChoice.create(it, it.lowercase()) })

			}
			else -> {
				autocompleteInteraction.respondWithChoices(listOf())
			}
		}

	}

}
