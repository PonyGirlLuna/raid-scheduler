package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.EventDefinition
import com.lunamatthews.raidscheduler.data.validDifficulties
import com.lunamatthews.raidscheduler.slashcommands.AutocompleteListener
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.*
import kotlin.jvm.optionals.getOrDefault
import kotlin.jvm.optionals.getOrNull

class ManageEventsCommand(private val eventDefinitionDb: JsonDB<EventDefinition>) : GuildSlashCommand("events", "Manages events for the bot"), AutocompleteListener {

	private val log: Logger = LogManager.getLogger(ManageEventsCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		//TODO Difficulty documentation (asterisk for defaults, "none" for no difficulty options)

		if (commandVersion < 7) {
			SlashCommand.with(name, description, listOf(
				SlashCommandOption.create(SlashCommandOptionType.SUB_COMMAND, "list", "Shows a list of all currently configured events"),
				SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "add", "Adds an event to the list of choices used when scheduling", listOf(
					SlashCommandOption.create(SlashCommandOptionType.STRING, "name", "The name of the event.", true),
					SlashCommandOption.create(SlashCommandOptionType.LONG, "min-participants", "The minimum number of participants for this event.", true),
					SlashCommandOption.create(SlashCommandOptionType.LONG, "max-participants", "The maximum number of participants for this event.", true),
					SlashCommandOption.create(SlashCommandOptionType.STRING, "color", "A splash of color to keep things lively. Should be a hex value (like #b00b69)!", true),
					SlashCommandOption.create(SlashCommandOptionType.STRING, "difficulties", "A comma separated list of difficulties (like expert,grandmaster). Use \"none\" for no options.", true),
					SlashCommandOption.create(SlashCommandOptionType.STRING, "icon", "A link to an icon to be displayed alongside the event. Preferably a square image.")
				)),
				SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND_GROUP, "edit", "Changes a single value of an existing event", listOf(
					SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "name", "Change the name of an event", listOf(
						SlashCommandOption.createStringOption("event", "The *current* name of the event.", true, true),
						SlashCommandOption.create(SlashCommandOptionType.STRING, "name", "The new name of the event.", true),
					)),
					SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "min-participants", "Change the minimum number of participants in an event", listOf(
						SlashCommandOption.createStringOption("event", "The name of the event.", true, true),
						SlashCommandOption.create(SlashCommandOptionType.LONG, "min-participants", "The minimum number of participants for this event.", true),
					)),
					SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "max-participants", "Change the maximum number of participants in an event", listOf(
						SlashCommandOption.createStringOption("event", "The name of the event.", true, true),
						SlashCommandOption.create(SlashCommandOptionType.LONG, "max-participants", "The maximum number of participants for this event.", true),
					)),
					SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "color", "Change the color displayed for an event", listOf(
						SlashCommandOption.createStringOption("event", "The name of the event.", true, true),
						SlashCommandOption.create(SlashCommandOptionType.STRING, "color", "A splash of color to keep things lively. Should be a hex value (like #b00b69)!", true),
					)),
					SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "difficulties", "Change the available difficulties for an event", listOf(
						SlashCommandOption.createStringOption("event", "The name of the event.", true, true),
						SlashCommandOption.create(SlashCommandOptionType.STRING, "difficulties", "A comma separated list of difficulties (like advanced,grandmaster). Use \"none\" for no options.", true),
					)),
					SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "icon", "Change the icon for an event", listOf(
						SlashCommandOption.createStringOption("event", "The name of the event.", true, true),
						SlashCommandOption.create(SlashCommandOptionType.STRING, "icon", "A link to an icon to be displayed alongside the event. Preferably a square image.", true)
					)),
					SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "info-link", "Change the info/forum link for an event", listOf(
						SlashCommandOption.createStringOption("event", "The name of the event.", true, true),
						SlashCommandOption.create(SlashCommandOptionType.STRING, "info-link", "A link to the forum post with infographics for this event.", false)
					))
				)),
				SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "remove", "Remove an event from the available choices", listOf(
					SlashCommandOption.createStringOption("event", "The name of the event to delete.", true, true)
				)),
			))
				.setDefaultEnabledForPermissions(PermissionType.MODERATE_MEMBERS)
				.createForServer(server)
				.join()
		}

	}

	@OptIn(ExperimentalStdlibApi::class)
	override fun handleEvent(event: SlashCommandCreateEvent) {

		val interaction = event.slashCommandInteraction
		val commandOptions = event.slashCommandInteraction.options[0]
		val serverId = interaction.server.get().id

		when (commandOptions.name) {
			"list" -> {

				val eventDefinitions = eventDefinitionDb.getAllValues<EventDefinition>().filter { it.server == serverId }

				val eventString = eventDefinitions.joinToString("\n") {
					if (it.minParticipants == it.maxParticipants) {
						"• ${it.name} (${it.minParticipants})"
					} else {
						"• ${it.name} (${it.minParticipants}-${it.maxParticipants})"
					}
				}
				interaction.createImmediateResponder().setContent("**The currently configured (${eventDefinitions.size}) events are:**\n$eventString").respond()

			}
			"add" -> {

				val name = commandOptions.getArgumentStringValueByName("name").get()
				val minParticipants = commandOptions.getArgumentLongValueByName("min-participants").get()
				val maxParticipants = commandOptions.getArgumentLongValueByName("max-participants").getOrDefault(minParticipants)
				val rawColor = commandOptions.getArgumentStringValueByName("color").get().lowercase()
				val icon = commandOptions.getArgumentStringValueByName("icon").getOrNull()
				val difficulties = commandOptions.getArgumentStringValueByName("difficulties").getOrDefault("")
				val infoLink = commandOptions.getArgumentStringValueByName("info-link").getOrNull()

				val color = if (rawColor.startsWith("#")) {
					rawColor
				} else {
					"#$rawColor"
				}

				val difficultyList = try {

					validateAndFormatDifficulties(difficulties)

				} catch (e: IllegalArgumentException) {

					interaction.createImmediateResponder()
						.setFlags(MessageFlag.EPHEMERAL)
						.setContent("Invalid difficulty list: `$difficulties`. ${e.message}")
						.respond()

					null

				}

				if (!color.matches(Regex("#[0-9a-z]{6}"))) {

					interaction.createImmediateResponder()
						.setFlags(MessageFlag.EPHEMERAL)
						.setContent("Invalid color code: `$color`. Must be a hexadecimal color value (like `#b00b69`).")
						.respond()

				} else if (eventDefinitionDb.hasValue { it.get("server").asLong() == serverId && it.get("name").asText() == name }) {

					interaction.createImmediateResponder()
						.setFlags(MessageFlag.EPHEMERAL)
						.setContent("Event with name `$name` already exists!")
						.respond()

				} else if (minParticipants > maxParticipants) {

					interaction.createImmediateResponder()
						.setFlags(MessageFlag.EPHEMERAL)
						.setContent("Minimum participants ($minParticipants) cannot be greater than maximum participants ($maxParticipants).")
						.respond()

				} else if (difficultyList != null) {

					val defaultDifficulty = if (difficultyList.size == 1) {
						difficultyList[0]
					} else {
						difficulties.split(",").firstOrNull { it.startsWith("*") || it.endsWith("*") }?.removePrefix("*")?.removeSuffix("*") ?: validDifficulties[0]
					}

					if (icon != null) {

						eventDefinitionDb.addValue(EventDefinition(serverId, name, minParticipants.toInt(), maxParticipants.toInt(), color, icon, infoLink, difficultyList, defaultDifficulty))

						interaction.createImmediateResponder()
							.setContent("Added $name ($minParticipants-$maxParticipants).")
							.respond()

					} else {

						eventDefinitionDb.addValue(EventDefinition(serverId, name, minParticipants.toInt(), maxParticipants.toInt(), color, null, infoLink, difficultyList, defaultDifficulty))

						interaction.createImmediateResponder()
							.setContent("Added $name ($minParticipants-$maxParticipants).")
							.respond()

					}

				}

			}
			"edit" -> {

				val editType = commandOptions.options[0].name
				val eventName = commandOptions.options[0].options[0].stringValue.get()
				val newValue = commandOptions.options[0].options[1].stringRepresentationValue.get()

				val currentEvents = eventDefinitionDb.getAllValues<EventDefinition>().filter { it.server == serverId }

				if (currentEvents.none { it.name == eventName }) {
					interaction.createImmediateResponder()
						.setContent("An existing event with the name `$eventName` could not be found.")
						.setFlags(MessageFlag.EPHEMERAL)
						.respond()
				}

				val eventToEdit = currentEvents.single { it.name == eventName }
				var successMessage = ""

				val output = when (editType) {
					"name" -> {

						if (eventDefinitionDb.hasValue { it.get("server").asLong() == serverId && it.get("name").asText() == name }) {

							interaction.createImmediateResponder()
								.setFlags(MessageFlag.EPHEMERAL)
								.setContent("Event with name `$name` already exists!")
								.respond()

							null

						} else {
							successMessage = "Name: `${eventToEdit.name}` -> `$newValue`"
							EventDefinition(eventToEdit.server, newValue, eventToEdit.minParticipants, eventToEdit.maxParticipants, eventToEdit.color, eventToEdit.icon, eventToEdit.infoLink, eventToEdit.difficulties)
						}

					}
					"min-participants" -> {

						val minParticipants = newValue.toInt()

						if (minParticipants > eventToEdit.maxParticipants) {

							interaction.createImmediateResponder()
								.setFlags(MessageFlag.EPHEMERAL)
								.setContent("Minimum participants ($minParticipants) cannot be greater than maximum participants (${eventToEdit.maxParticipants}).")
								.respond()

							null

						} else {
							successMessage = "Minimum participants: `${eventToEdit.minParticipants}` -> `$minParticipants`"
							EventDefinition(eventToEdit.server, eventToEdit.name, minParticipants, eventToEdit.maxParticipants, eventToEdit.color, eventToEdit.icon, eventToEdit.infoLink, eventToEdit.difficulties)
						}

					}
					"max-participants" -> {

						val maxParticipants = newValue.toInt()

						if (maxParticipants < eventToEdit.minParticipants) {

							interaction.createImmediateResponder()
								.setFlags(MessageFlag.EPHEMERAL)
								.setContent("Maximum participants ($maxParticipants) cannot be less than minimum participants (${eventToEdit.minParticipants}).")
								.respond()

							null

						} else {
							successMessage = "Maximum participants: `${eventToEdit.maxParticipants}` -> `$newValue`"
							EventDefinition(eventToEdit.server, eventToEdit.name, eventToEdit.minParticipants, maxParticipants, eventToEdit.color, eventToEdit.icon, eventToEdit.infoLink, eventToEdit.difficulties)
						}

					}
					"color" -> {

						val color = if (newValue.startsWith("#")) {
							newValue
						} else {
							"#$newValue"
						}.lowercase()

						if (!color.matches(Regex("#[0-9a-z]{6}"))) {

							interaction.createImmediateResponder()
								.setFlags(MessageFlag.EPHEMERAL)
								.setContent("Invalid color code: `$color`. Must be a hexadecimal color value (like `#b000b5`).")
								.respond()

							null

						} else {
							successMessage = "Color: `${eventToEdit.color}` -> `$color`"
							EventDefinition(eventToEdit.server, eventToEdit.name, eventToEdit.minParticipants, eventToEdit.maxParticipants, color, eventToEdit.icon, eventToEdit.infoLink, eventToEdit.difficulties)
						}

					}
					"difficulties" -> {

						try {

							val difficulties = validateAndFormatDifficulties(newValue)
							successMessage = "Difficulties: `${eventToEdit.difficulties}` -> `$difficulties`"
							EventDefinition(eventToEdit.server, eventToEdit.name, eventToEdit.minParticipants, eventToEdit.maxParticipants, eventToEdit.color, eventToEdit.icon, eventToEdit.infoLink, difficulties)

						} catch (e: IllegalArgumentException) {

							interaction.createImmediateResponder()
								.setFlags(MessageFlag.EPHEMERAL)
								.setContent("Invalid difficulty list: `$newValue`. ${e.message}")
								.respond()

							null

						}

					}
					"icon" -> {
						successMessage = "Icon: `${eventToEdit.icon}` -> `$newValue`"
						EventDefinition(eventToEdit.server, eventToEdit.name, eventToEdit.minParticipants, eventToEdit.maxParticipants, eventToEdit.color, newValue, eventToEdit.infoLink, eventToEdit.difficulties)
					}
					"info-link" -> {
						successMessage = "Info Link: `${eventToEdit.infoLink}` -> `$newValue`"
						EventDefinition(eventToEdit.server, eventToEdit.name, eventToEdit.minParticipants, eventToEdit.maxParticipants, eventToEdit.color, eventToEdit.icon, newValue, eventToEdit.difficulties)
					}
					else -> {
						null
					}
				}

				if (output != null) {

					eventDefinitionDb.updateValue({ it.get("name").asText() == eventToEdit.name && it.get("server").asLong() == eventToEdit.server }, output)

					interaction.createImmediateResponder()
						.setContent("Event `${eventToEdit.name}` updated:\n\t$successMessage")
						.respond()

				} else {
					log.error("Unrecognized update option '$editType' when updating an event.")
					interaction.createImmediateResponder()
						.setContent("Unrecognized update option '$editType'. How did you even do this? (Let Luna know!)")
						.setFlags(MessageFlag.EPHEMERAL)
						.respond()
				}

			}
			"remove" -> {

				val name = commandOptions.getArgumentStringValueByName("event").get()

				if (!eventDefinitionDb.hasValue { it.get("server").asLong() == serverId && it.get("name").asText() == name }) {

					interaction.createImmediateResponder()
						.setFlags(MessageFlag.EPHEMERAL)
						.setContent("Event with name `$name` could not be found.")
						.respond()

				}

				eventDefinitionDb.deleteValue { it.get("server").asLong() == serverId && it.get("name").asText() == name }

				interaction.createImmediateResponder()
					.setContent("Removed event `$name`.")
					.respond()

			}
		}

	}

	private fun validateAndFormatDifficulties(difficultyList: String): List<String> {

		if (difficultyList.isEmpty()) {
			return listOf()
		}

		val difficulties = difficultyList.split(",").map {
			it.trim().lowercase().removePrefix("*").removeSuffix("*")
		}.filter { it.isNotBlank() && it != "none" }

		val invalidDifficulties = difficulties.filter { !validDifficulties.contains(it) }

		if (invalidDifficulties.isNotEmpty()) {
			throw IllegalArgumentException("Unknown difficulties: ${invalidDifficulties.joinToString(", "){"`$it`"}}.")
		} else if (difficulties.count { it.startsWith("*") || it.endsWith("*") } > 1) {
			throw IllegalArgumentException("Multiple difficulties have been given a default value. Only one may be the default.")
		}

		return difficulties

	}

	override fun handleAutocompleteInteraction(autocompleteInteraction: AutocompleteInteraction) {

		val server = autocompleteInteraction.server.get().id
		val interactionName = autocompleteInteraction.focusedOption.name
		val value = autocompleteInteraction.focusedOption.stringValue.get()

		if (interactionName == "event") {

			val availableEvents = eventDefinitionDb.getAllValues<EventDefinition>().filter { it.server == server }

			autocompleteInteraction.respondWithChoices(availableEvents.filter { it.name.startsWith(value, true) }.take(25).map {
				SlashCommandOptionChoice.create(it.name, it.name)
			})

		}

	}

}
