package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent

class CommitEventChangesCommand(private val scheduleEventCommand: ScheduleEventCommand) : GuildSlashCommand("commit", "Commit activity changes for the bot") {

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		/*if (commandVersion < 1) {
			SlashCommand.with(name, description)
				.setDefaultEnabledForPermissions(PermissionType.MODERATE_MEMBERS)
				.createForServer(server)
				.join()
		}*/

		//if (commandVersion < 7) {
		server.slashCommands.get().find { it.name == name }?.delete()?.get()
		//}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		//TODO Remove this; deprecated

		/*val followUpMessageBuilder = event.interaction.respondLater().get()

		scheduleEventCommand.updateEventOptions(event.interaction.server.get())

		followUpMessageBuilder.setContent("Available activities updated.").update()*/

	}

}
