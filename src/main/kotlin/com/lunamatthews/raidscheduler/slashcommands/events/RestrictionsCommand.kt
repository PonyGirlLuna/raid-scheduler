package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.BannedUserList
import com.lunamatthews.raidscheduler.data.UserBan
import com.lunamatthews.raidscheduler.util.getBannedUserListByServerId
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand

class RestrictionsCommand(private val bannedUserListDb: JsonDB<BannedUserList>) : GuildSlashCommand("restrictions", "Lists all active restrictions.") {

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 5) {
			SlashCommand.with(name, description)
				.setDefaultEnabledForPermissions(PermissionType.MODERATE_MEMBERS)
				.createForServer(server)
				.join()
		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val bans = getBannedUserListByServerId(bannedUserListDb, event.slashCommandInteraction.server.get().id).bans

		if (bans.isEmpty()) {

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("There are no active restrictions.")
				.respond()

		} else {

			//TODO Handle more than 10 bans (embed limit)
			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.addEmbeds(bans.map { buildEmbed(it) })
				.respond()

		}

	}

	private fun buildEmbed(userBan: UserBan): EmbedBuilder {

		val bannedUser = bot.api.getUserById(userBan.user).get()

		return EmbedBuilder()
			.setTitle(bannedUser.discriminatedName)
			.addInlineField("Restricted by:", "<@${userBan.modWhoBanned}>")
			.addInlineField("Start:", "<t:${userBan.start}:f>")
			.addInlineField("Expires on:", "<t:${userBan.expiration}:f>")
			.addField("Note:", userBan.note)

	}

}
