package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.raidscheduler.exceptions.EventDoesNotExistException
import com.lunamatthews.raidscheduler.exceptions.UserDoesNotHavePermissionException
import com.lunamatthews.raidscheduler.services.EventManager
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent

class RenameThreadCommand(private val eventManager: EventManager) : GuildSlashCommand("rename", "Renames a thread for the given event.") {

	private val log: Logger = LogManager.getLogger(RenameThreadCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 4) {
			/*SlashCommand
				.with(name, description, listOf(
					SlashCommandOption.create(SlashCommandOptionType.STRING, "event", "The ID of the event for which to change the thread name", true),
					SlashCommandOption.create(SlashCommandOptionType.STRING, "name", "The new name for the thread", true)
				))
				.createForServer(server)
				.join()*/
		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val server = event.interaction.server.get()

		val eventId = event.slashCommandInteraction.getArgumentStringValueByName("event").get()
		val name = event.slashCommandInteraction.getArgumentStringValueByName("name").get()

		try {

			eventManager.renameThread(eventId, name, event.interaction.user, server)

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setContent("Thread for event `$eventId` renamed.")
				.respond()

		} catch (e: EventDoesNotExistException) {

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("Could not find event with ID `$eventId`.")
				.respond()

		} catch (e: UserDoesNotHavePermissionException) {

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("You did not create this event and do not have permission to reserve spots for this event.")
				.respond()

		}

	}

}
