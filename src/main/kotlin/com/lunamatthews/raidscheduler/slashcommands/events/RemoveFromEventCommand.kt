package com.lunamatthews.raidscheduler.slashcommands.events

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.Configuration
import com.lunamatthews.raidscheduler.data.ScheduledEvent
import com.lunamatthews.raidscheduler.exceptions.EventDoesNotExistException
import com.lunamatthews.raidscheduler.exceptions.UserNotInEventException
import com.lunamatthews.raidscheduler.services.EventManager
import com.lunamatthews.raidscheduler.util.getConfigurationByServerId
import com.lunamatthews.raidscheduler.util.getScheduledEventById
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionType

class RemoveFromEventCommand(private val eventManager: EventManager,
                             private val scheduledEventDb: JsonDB<ScheduledEvent>,
                             private val configurationDb: JsonDB<Configuration>) : GuildSlashCommand("remove", "Forcibly removes a user from an event—THERE IS NO UNDO") {

	private val log: Logger = LogManager.getLogger(RemoveFromEventCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 1) {
			SlashCommand.with(name, description, listOf(
				SlashCommandOption.create(SlashCommandOptionType.USER, "user", "The user to remove from the specified event", true),
				SlashCommandOption.create(SlashCommandOptionType.STRING, "event-id", "The unique ID of the event from which to remove the user", true)
			))
				.setDefaultEnabledForPermissions(PermissionType.MODERATE_MEMBERS)
				.createForServer(server)
				.join()
		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val server = event.interaction.server.get()
		val user = event.slashCommandInteraction.getArgumentUserValueByName("user").get()
		val eventId = event.slashCommandInteraction.getArgumentStringValueByName("event-id").get()

		val configuration = getConfigurationByServerId(configurationDb, server.id)

		try {

			eventManager.removeUserFromEvent(eventId, user.id, server.id)

			val activity = getScheduledEventById(scheduledEventDb, eventId)

			MessageBuilder()
				.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
				.setContent("**Moderation Action:** <@${event.slashCommandInteraction.user.id}> has removed <@${user.id}> from event `$eventId` (${activity.definition.name} at <t:${activity.time}:F>).")
				.send(server.getChannelById(configuration.modChannel!!).get().asTextChannel().get())

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("Removed <@${user.id}> from event `$eventId` (${activity.definition.name} at <t:${activity.time}:F>).")
				.respond()

		} catch (e: UserNotInEventException) {
			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("User ${user.getDisplayName(server)} is not participating in the event, so they could not be removed from it!")
				.respond()
		} catch (e: EventDoesNotExistException) {
			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("Could not find event with ID `$eventId`.")
				.respond()
		}


	}

}
