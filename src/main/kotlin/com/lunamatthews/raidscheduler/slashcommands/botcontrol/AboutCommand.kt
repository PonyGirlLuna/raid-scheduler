package com.lunamatthews.raidscheduler.slashcommands.botcontrol

import com.lunamatthews.discordbotcore.commands.GlobalSlashCommand
import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand
import java.awt.Color

class AboutCommand : GlobalSlashCommand("about", "Displays information about the bot itself") {

    override fun createOrUpdateCommand(commandVersion: Int) {

        if (commandVersion < 1) {
            SlashCommand.with(name, description)
                .createGlobal(bot.api)
                .join()
        }

    }

    override fun handleEvent(event: SlashCommandCreateEvent) {

        val embed = EmbedBuilder()
            .setTitle("Raidbot " + bot.getVersion())
            .setDescription("Raidbot handles scheduling and RSVPs for Destiny 2 events.")
            .addField("Source Code", "https://gitlab.com/PonyGirlLuna/raid-scheduler")
            .setFooter("Created by Luna")
            .setColor(Color.decode("#b50079"))

        event.slashCommandInteraction.createImmediateResponder()
            .addEmbed(embed)
            .respond()

    }

}
