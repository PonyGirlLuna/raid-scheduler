package com.lunamatthews.raidscheduler.slashcommands.botcontrol

import com.lunamatthews.discordbotcore.commands.GlobalSlashCommand
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand

class InviteCommand : GlobalSlashCommand("invite", "Generates an invite link") {

	override fun createOrUpdateCommand(commandVersion: Int) {

		if (commandVersion < 1) {
			SlashCommand.with(name, description)
				.setDefaultEnabledForPermissions(PermissionType.ADMINISTRATOR)
				.createGlobal(bot.api)
				.join()
		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {
		if (event.slashCommandInteraction.user.isBotOwnerOrTeamMember) {
			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent(bot.createInvite()).respond()
		} else {
			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent("This is not a public bot (yet?)—you must be an owner of this bot to create an invite.").respond()
		}
	}

}
