package com.lunamatthews.raidscheduler.slashcommands.botcontrol

import com.lunamatthews.discordbotcore.commands.GlobalSlashCommand
import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand
import java.awt.Color

class HelpCommand : GlobalSlashCommand("help", "Information about scheduling events using Raidbot") {

    override fun createOrUpdateCommand(commandVersion: Int) {

        if (commandVersion < 1) {
            SlashCommand.with(name, description)
                .createGlobal(bot.api)
                .join()
        }

    }

    override fun handleEvent(event: SlashCommandCreateEvent) {

        val embed = EmbedBuilder()
            .setTitle("Using the Raid Scheduler")
            .setDescription("Despite its name, this bot can be used to schedule any team-based activity in Destiny 2—not just raids!")
            .addField("Scheduling an Event", "To schedule an event, just type `/schedule` and follow the prompts to input which event and when it should take place. See below for accepted time and date formats. You will have a chance to check your post before it is made available to everyone. ***By default, all scheduling takes place in EST/EDT, but you can configure this (see below).***")
            .addField("Canceling an Event", "Once you've scheduled an event, you can cancel it using `/cancel`, but please use this sparingly. It will not notify anyone that your event has been canceled, so be sure to let everyone who was going to participate know (or have someone else take over).")
            .addField("Pinging Participants", "When it comes time for your event, you may want to ping everyone who's signed up. To do this, just use `/ping` and the main team will be notified. If you need a fill, just use `/fill` and everyone else will get a ping. Please be mindful of pinging people too much; generally a single use of each command will do.")
            .addField("Making a Reservation", "If you want to schedule an event for someone in particular, go ahead and schedule as normal, then use `/reservation add <user> <eventId>` to guarantee them a spot (this will kick out existing spleebs if there's not enough room, so be sparing, please). The eventId can be found at the bottom of the event post.")
            .addField("Removing a Reservation", "If you no longer want to reserve a spot for someone, simply use `/reservation remove <user> <eventId>.")
            .addField("Setting your Settings", "The bot can handle time zone conversion for you so you can schedule in your own time zone. Use `/timezones` to locate an appropriate time zone code, then use `/settings set Time Zone <code>`. Additional settings may be added in the future if the need arises.")
            .addField("Date Formats", "The bot accepts the following date formats:\n" +
                    "• Relative times (`today`, `tonight`, `tomorrow`)\n" +
                    "• Day of the week (`sunday`/`sun`, `thursday`/`thu`, etc)\n" +
                    "• Month/Day (`9/26`)\n" +
                    "• Month/Day/Year (`9/26/23`)")
            .addField("Time Formats", "The bot accepts the following time formats:\n" +
                    "• Convenient times (`now`, `noon`, `midnight`)\n" +
                    "• Relative times (`in 15 minutes`, `35m`, `26mins`, `2 hours`)\n" +
                    "• Absolute times (`7:30`, `6am`, `9`, `8:35 pm`, `9:45pm`)\n" +
                    "• **Times will assume PM;** specify \"am\" if scheduling before noon.")
            .setColor(Color.decode("#a5dd5c"))

        event.slashCommandInteraction.createImmediateResponder()
            .addEmbed(embed)
            .respond()

    }

}
