package com.lunamatthews.raidscheduler.slashcommands

import org.javacord.api.entity.user.User
import org.javacord.api.interaction.ButtonInteraction

interface ButtonListener {
	fun buttonIds(): List<String>
	fun handleButtonInteraction(buttonInteraction: ButtonInteraction, interactionType: String, id: String, user: User)
}
