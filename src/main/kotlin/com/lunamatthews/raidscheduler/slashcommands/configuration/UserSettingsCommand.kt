package com.lunamatthews.raidscheduler.slashcommands.configuration

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.UserSettings
import com.lunamatthews.raidscheduler.util.getUserSettingsById
import com.lunamatthews.raidscheduler.util.saveUserSettings
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionChoice
import org.javacord.api.interaction.SlashCommandOptionType
import java.awt.Color
import java.time.ZoneId

class UserSettingsCommand(private val userSettingsDb: JsonDB<UserSettings>) : GuildSlashCommand("settings", "View and configure various settings for yourself") {

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 4) {

			SlashCommand.with(name, description)
				.setOptions(
					listOf(
						SlashCommandOption.create(SlashCommandOptionType.SUB_COMMAND, "list", "Shows your current settings"),
						SlashCommandOption.createWithOptions(
							SlashCommandOptionType.SUB_COMMAND, "help", "Provides additional details about the selected setting", listOf(
								SlashCommandOption.createWithChoices(
									SlashCommandOptionType.STRING, "setting", "The setting for which to display a help message", true, listOf(
										SlashCommandOptionChoice.create("Time Zone", "time-zone"),
									)
								)
							)
						),
						SlashCommandOption.createWithOptions(
							SlashCommandOptionType.SUB_COMMAND, "set", "Sets the value of a setting", listOf(
								SlashCommandOption.createWithChoices(
									SlashCommandOptionType.STRING, "setting", "The setting for which to set a value", true, listOf(
										SlashCommandOptionChoice.create("Time Zone", "time-zone"),
									)
								),
								SlashCommandOption.create(SlashCommandOptionType.STRING, "value", "The value you want to set for this setting", true)
							)
						),
						SlashCommandOption.createWithOptions(
							SlashCommandOptionType.SUB_COMMAND, "unset", "Removes the value of a setting, restoring it to its default value", listOf(
								SlashCommandOption.createWithChoices(
									SlashCommandOptionType.STRING, "setting", "The setting for which to return to a default value", true, listOf(
										SlashCommandOptionChoice.create("Time Zone", "time-zone"),
									)
								)
							)
						)
					)
				)
				.createForServer(server)
				.join()

		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		val user = event.slashCommandInteraction.user
		val option = event.slashCommandInteraction.options[0].name

		val userSettings = getUserSettingsById(userSettingsDb, user.id)

		if (option == "list") {

			val notConfigured = "*`Not Configured`*"

			val timeZone = userSettings.timeZone?.let { "`$it`" } ?: notConfigured

			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.addEmbed(
				EmbedBuilder()
					.addField("Time zone:", timeZone, true)
					.setColor(Color.decode("#b1678c"))
					.setTimestampToNow()
			).respond()

			return

		}

		val property = event.slashCommandInteraction.arguments[0].stringValue.get()

		if (option == "help") {

			val helpMessage = when (property) {
				"time-zone" -> {
					"**Time zone:**\nThis is the time zone that will be used for you whenever you schedule events. See a list of all supported time zones with `/timezones`."
				}

				else -> {
					"Unknown Option $property. Please let Luna know! (This should never happen.)"
				}
			}

			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent(helpMessage).respond()

			return

		}

		var changed = false
		var error = false
		var message = ""

		if (option == "unset") {

			when (property) {

				"time-zone" -> {

					userSettings.timeZone = null
					message += "**Time zone** unset. Now using the default of `America/New_York`."
					changed = true

				}

			}

		} else {

			val argument = event.slashCommandInteraction.arguments[1]

			when (property) {

				"time-zone" -> {

					val value = argument.stringValue.get()

					if (value in ZoneId.getAvailableZoneIds()) {
						userSettings.timeZone = value
						message += "**Time zone** set to `$value`"
						changed = true
					} else {
						message += "Unknown time zone `$value`. See a list of supported time zones with `/timezones`."
						error = true
					}
				}

			}

		}

		if (error) {
			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent(message).respond()
		} else if (changed) {
			saveUserSettings(userSettingsDb, userSettings)
			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent(message).respond()
		} else {
			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent("No changes applied.").respond()
		}

	}

}
