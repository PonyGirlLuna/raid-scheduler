package com.lunamatthews.raidscheduler.slashcommands.configuration

import com.lunamatthews.discordbotcore.commands.GuildSlashCommand
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.Configuration
import com.lunamatthews.raidscheduler.util.getConfigurationByServerId
import com.lunamatthews.raidscheduler.util.saveConfiguration
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.channel.Channel
import org.javacord.api.entity.channel.ChannelType
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.entity.server.Server
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.SlashCommand
import org.javacord.api.interaction.SlashCommandOption
import org.javacord.api.interaction.SlashCommandOptionChoice
import org.javacord.api.interaction.SlashCommandOptionType
import java.awt.Color
import kotlin.jvm.optionals.getOrNull

class ConfigurationCommand(private val configurationDB: JsonDB<Configuration>) : GuildSlashCommand("configuration", "Configure various things about the bot") {

	private val log: Logger = LogManager.getLogger(ConfigurationCommand::class.java)

	override fun createOrUpdateCommand(server: Server, commandVersion: Int) {

		if (commandVersion < 7) {

			SlashCommand.with(name, description)
				.setOptions(
					listOf(
						SlashCommandOption.create(SlashCommandOptionType.SUB_COMMAND, "list", "Shows the current bot configuration for this server"),
						SlashCommandOption.createWithOptions(
							SlashCommandOptionType.SUB_COMMAND, "help", "Provides additional details about the selected property", listOf(
								SlashCommandOption.createWithChoices(
									SlashCommandOptionType.STRING, "property", "The property to display a help message for", true, listOf(
										SlashCommandOptionChoice.create("Event Role", "activity-role"),
										SlashCommandOptionChoice.create("Moderation Channel", "mod-channel"),
										SlashCommandOptionChoice.create("Primary Event Channel", "event-channel"),
										SlashCommandOptionChoice.create("Secondary Event Channel", "secondary-event-channel"),
										SlashCommandOptionChoice.create("Confirm Event Creation Emote", "confirm-event-creation-emote"),
										SlashCommandOptionChoice.create("Cancel Event Creation Emote", "cancel-event-creation-emote"),
										SlashCommandOptionChoice.create("Join Event Emote", "join-event-emote"),
										SlashCommandOptionChoice.create("Maybe Join Event Emote", "maybe-join-event-emote"),
										SlashCommandOptionChoice.create("Remove From Event Emote", "remove-from-event-emote"),
									)
								)
							)
						),
						SlashCommandOption.createWithOptions(
							SlashCommandOptionType.SUB_COMMAND, "set-role", "Sets a value for a role-based property", listOf(
								SlashCommandOption.createWithChoices(
									SlashCommandOptionType.STRING, "property", "The property to set", true, listOf(
										SlashCommandOptionChoice.create("Event Role", "activity-role")
									)
								),
								SlashCommandOption.create(SlashCommandOptionType.ROLE, "value", "The role to use", true)
							)
						),
						SlashCommandOption.createWithOptions(
							SlashCommandOptionType.SUB_COMMAND, "set-channel", "Sets a value for a channel-based property", listOf(
								SlashCommandOption.createWithChoices(
									SlashCommandOptionType.STRING, "property", "The property to set", true, listOf(
										SlashCommandOptionChoice.create("Moderation Channel", "mod-channel"),
										SlashCommandOptionChoice.create("Event Channel", "event-channel"),
										SlashCommandOptionChoice.create("Secondary Event Channel", "secondary-event-channel"),
									)
								),
								SlashCommandOption.create(SlashCommandOptionType.CHANNEL, "value", "The channel to use", true)
							)
						),
						SlashCommandOption.createWithOptions(
							SlashCommandOptionType.SUB_COMMAND, "set-emote", "Sets a value for an emote-based property", listOf(
								SlashCommandOption.createWithChoices(
									SlashCommandOptionType.STRING, "property", "The property to set", true, listOf(
										SlashCommandOptionChoice.create("Confirm Event Creation Emote", "confirm-event-creation-emote"),
										SlashCommandOptionChoice.create("Cancel Event Creation Emote", "cancel-event-creation-emote"),
										SlashCommandOptionChoice.create("Join Event Emote", "join-event-emote"),
										SlashCommandOptionChoice.create("Maybe Join Event Emote", "maybe-join-event-emote"),
										SlashCommandOptionChoice.create("Remove From Event Emote", "remove-from-event-emote"),
										SlashCommandOptionChoice.create("Standard Difficulty Emote", "standard-difficulty-emote"),
										SlashCommandOptionChoice.create("Advanced Difficulty Emote", "advanced-difficulty-emote"),
										SlashCommandOptionChoice.create("Expert Difficulty Emote", "expert-difficulty-emote"),
										SlashCommandOptionChoice.create("Master Difficulty Emote", "master-difficulty-emote"),
										SlashCommandOptionChoice.create("Grandmaster Difficulty Emote", "grandmaster-difficulty-emote"),
									)
								),
								SlashCommandOption.create(SlashCommandOptionType.STRING, "value", "The emote to use", true)
							)
						)
					)
				)
				.setDefaultEnabledForPermissions(PermissionType.MODERATE_MEMBERS)
				.createForServer(server)
				.join()

		}

	}

	@OptIn(ExperimentalStdlibApi::class)
	override fun handleEvent(event: SlashCommandCreateEvent) {

		val server = event.slashCommandInteraction.server.get()
		val option = event.slashCommandInteraction.options[0].name
		val configuration = getConfigurationByServerId(configurationDB, server.id)

		if (option == "list") {

			val notConfigured = "*`Not Configured`*"

			val modChannel = configuration.modChannel?.let { "`#" + server.getChannelById(it).get().name + "`" } ?: notConfigured
			val eventChannel = configuration.eventChannel?.let { "`#" + server.getChannelById(it).get().name + "`" } ?: notConfigured
			val secondaryEventChannel = configuration.secondaryEventChannel?.let { "`#" + server.getChannelById(it).getOrNull()?.name + "`" } ?: notConfigured
			val roleToPing = configuration.roleToPing?.let { "`@" + server.getRoleById(it).get().name + "`" } ?: notConfigured

			val confirmEventCreationEmote = configuration.confirmEventCreationEmote?.let { "`:$it:`" } ?: notConfigured
			val cancelEventCreationEmote = configuration.cancelEventCreationEmote?.let { "`:$it:`" } ?: notConfigured
			val spleebEmote = configuration.joinEventEmote?.let { "`:$it:`" } ?: notConfigured
			val maybeEmote = configuration.maybeJoinEventEmote?.let { "`:$it:`" } ?: notConfigured
			val removeEmote = configuration.removeFromEventEmote?.let { "`:$it:`" } ?: notConfigured

			val standardDifficultyEmote = configuration.difficultyEmoteStandard?.let { "`:$it:`" } ?: notConfigured
			val advancedDifficultyEmote = configuration.difficultyEmoteAdvanced?.let { "`:$it:`" } ?: notConfigured
			val expertDifficultyEmote = configuration.difficultyEmoteExpert?.let { "`:$it:`" } ?: notConfigured
			val masterDifficultyEmote = configuration.difficultyEmoteMaster?.let { "`:$it:`" } ?: notConfigured
			val grandmasterDifficultyEmote = configuration.difficultyEmoteGrandmaster?.let { "`:$it:`" } ?: notConfigured

			event.slashCommandInteraction.createImmediateResponder().addEmbed(
				EmbedBuilder()
					.addField("Moderation channel:", modChannel, true)
					.addField("Event channel:", eventChannel, true)
					.addField("Secondary Event channel:", secondaryEventChannel, true)
					.addField("Event role:", roleToPing, true)
					.addField("Confirm Event Creation emote:", confirmEventCreationEmote, false)
					.addField("Cancel Event Creation emote:", cancelEventCreationEmote, false)
					.addField("Join Event emote:", spleebEmote, false)
					.addField("Maybe Join Event emote:", maybeEmote, false)
					.addField("Remove From Event emote:", removeEmote, false)
					.addField("Standard Difficulty Emote:", standardDifficultyEmote, false)
					.addField("Advanced Difficulty Emote:", advancedDifficultyEmote, false)
					.addField("Expert Difficulty Emote:", expertDifficultyEmote, false)
					.addField("Master Difficulty Emote:", masterDifficultyEmote, false)
					.addField("Grandmaster Difficulty Emote:", grandmasterDifficultyEmote, false)
					.setColor(Color.decode("#8967b1"))
					.setTimestampToNow()
			).respond()

			return

		}

		val property = event.slashCommandInteraction.arguments[0].stringValue.get()

		if (option == "help") {

			val helpMessage = when (property) {
				"activity-role" -> {
					"**Event Ping role:**\nThis is the role that gets pinged whenever a new event is scheduled."
				}

				"mod-channel" -> {
					"**Moderation channel:**\nModeration actions will be logged to this channel."
				}

				"event-channel" -> {
					"**Primary scheduling channel:**\nEvents will be posted to this channel unless the schedule command is used in another allowed channel."
				}

				"secondary-event-channel" -> {
					"**Secondary event channel:**\nEvents are allowed to be posted to this channel."
				}

				"confirm-event-creation-emote" -> {
					"**Confirm Event Creation emote:**\nThe emote displayed on the button used to confirm that an event looks correct and should be posted."
				}

				"cancel-event-creation-emote" -> {
					"**Cancel Event Creation emote:**\nThe emote displayed on the button used to cancel creating an event."
				}

				"join-event-emote" -> {
					"**Join Event emote:**\nThe emote displayed on the button used to join the main team of an event."
				}

				"maybe-join-event-emote" -> {
					"**Maybe Join Event emote:**\nThe emote displayed on the button used to indicate potential availability to join an event."
				}

				"remove-from-event-emote" -> {
					"**Remove From Event emote:**\nThe emote displayed on the button used to remove one from an event."
				}

				"standard-difficulty-emote" -> {
					"**Standard Difficulty Emote:**\nThe emote displayed for standard difficulty events."
				}

				"advanced-difficulty-emote" -> {
					"**Advanced Difficulty Emote:**\nThe emote displayed for advanced difficulty events."
				}

				"expert-difficulty-emote" -> {
					"**Expert Difficulty Emote:**\nThe emote displayed for expert difficulty events."
				}

				"master-difficulty-emote" -> {
					"**Master Difficulty Emote:**\nThe emote displayed for master difficulty events."
				}

				"grandmaster-difficulty-emote" -> {
					"**Grandmaster Difficulty Emote:**\nThe emote displayed for grandmaster difficulty events."
				}

				else -> {
					"Unknown Option $property. Please let Luna know! (This should never happen.)"
				}

			}

			event.slashCommandInteraction.createImmediateResponder().setFlags(MessageFlag.EPHEMERAL).setContent(helpMessage).respond()

			return

		}

		val responder = event.slashCommandInteraction.respondLater()
		val argument = event.slashCommandInteraction.arguments[1]
		var changed = false
		var error = false
		var message = ""

		when (property) {
			"activity-role" -> {
				val value = argument.roleValue.get()
				configuration.roleToPing = value.id
				message += "**Event Ping role** set to `@${value.name}`"
				changed = true
			}

			"mod-channel" -> {
				val value = argument.channelValue.get()
				if (validateChannel(value)) {
					configuration.modChannel = value.id
					message += "**Moderation channel** set to `#${value.name}`"
					changed = true
				} else {
					message += "Invalid channel provided for **Moderation channel**. Must be an accessible text channel."
					error = true
				}
			}

			"event-channel" -> {
				val value = argument.channelValue.get()
				if (validateChannel(value)) {
					configuration.eventChannel = value.id
					message += "**Event Scheduling channel** set to `#${value.name}`"
					changed = true
				} else {
					message += "Invalid channel provided for **Event Scheduling channel**. Must be an accessible text channel."
					error = true
				}
			}

			"secondary-event-channel" -> {
				val value = argument.channelValue.get()
				if (validateChannel(value)) {
					configuration.secondaryEventChannel = value.id
					message += "**Secondary Scheduling channel** set to `#${value.name}`"
					changed = true
				} else {
					message += "Invalid channel provided for **Secondary Scheduling channel**. Must be an accessible text channel."
					error = true
				}
			}

			"confirm-event-creation-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.confirmEventCreationEmote = value
					message += "**Confirm Event Creation emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

			"cancel-event-creation-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.cancelEventCreationEmote = value
					message += "**Cancel Event Creation emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

			"join-event-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.joinEventEmote = value
					message += "**Join Event emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

			"maybe-join-event-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.maybeJoinEventEmote = value
					message += "**Maybe Join Event emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

			"remove-from-event-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.removeFromEventEmote = value
					message += "**Remove From Event emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

			"standard-difficulty-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.difficultyEmoteStandard = value
					message += "**Standard Difficulty emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

			"advanced-difficulty-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.difficultyEmoteAdvanced = value
					message += "**Advanced Difficulty emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

			"expert-difficulty-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.difficultyEmoteExpert = value
					message += "**Expert Difficulty emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

			"master-difficulty-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.difficultyEmoteMaster = value
					message += "**Master Difficulty emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

			"grandmaster-difficulty-emote" -> {
				val value = parseEmote(argument.stringValue.get())
				if (validateEmote(value, server)) {
					configuration.difficultyEmoteGrandmaster = value
					message += "**Grandmaster Difficulty emote** set to `:$value:`"
					changed = true
				} else {
					message += "Could not find emote `:$value:`."
					error = true
				}
			}

		}

		if (error) {
			responder.get().setFlags(MessageFlag.EPHEMERAL).setContent(message).update()
		} else if (changed) {
			saveConfiguration(configurationDB, configuration)
			responder.get().setContent(message).update()
		} else {
			responder.get().setContent("No changes applied.").update()
		}

	}

	private fun validateChannel(channel: Channel): Boolean {
		return channel.type == ChannelType.SERVER_TEXT_CHANNEL
	}

	private fun validateEmote(emote: String, server: Server): Boolean {
		return server.getCustomEmojisByName(emote).isNotEmpty()
	}

	private fun parseEmote(value: String): String {
		return value.substringAfter(":").substringBefore(":")
	}

}
