package com.lunamatthews.raidscheduler.slashcommands.configuration

import com.lunamatthews.discordbotcore.commands.GlobalSlashCommand
import com.lunamatthews.raidscheduler.slashcommands.ButtonListener
import org.javacord.api.entity.message.component.ActionRowBuilder
import org.javacord.api.entity.message.component.Button
import org.javacord.api.entity.user.User
import org.javacord.api.event.interaction.SlashCommandCreateEvent
import org.javacord.api.interaction.ButtonInteraction
import org.javacord.api.interaction.SlashCommand
import java.time.ZoneId

class TimeZonesCommand : GlobalSlashCommand("timezones", "Displays a list of all time zones that the bot recognizes (for use in user settings)."), ButtonListener {

	private var cachedZones: List<String>? = null

	override fun createOrUpdateCommand(commandVersion: Int) {

		if (commandVersion < 4) {
			SlashCommand.with(name, description)
				.createGlobal(bot.api)
				.join()
		}

	}

	override fun handleEvent(event: SlashCommandCreateEvent) {

		/*if (event.slashCommandInteraction.server.isPresent) {
			event.slashCommandInteraction.createImmediateResponder()
				.setFlags(MessageFlag.EPHEMERAL)
				.setContent("This command may only be used in DMs. Please try again there!")
				.respond()
		}*/

		val zones = getZonePages()
		var output = zones[0]

		output += "\n\n*Page **1** of **${zones.size}***"

		event.slashCommandInteraction.createImmediateResponder()
			.setContent(output)
			.addComponents(ActionRowBuilder().addComponents(
				Button.secondary("time-zone-command-previous:1", "←"),
				Button.primary("time-zone-command-next:1", "→"),
				Button.danger("time-zone-command-done:0", "✕")
			).build())
			.respond()

	}

	override fun buttonIds(): List<String> {
		return listOf("time-zone-command-previous", "time-zone-command-next", "time-zone-command-done")
	}

	override fun handleButtonInteraction(buttonInteraction: ButtonInteraction, interactionType: String, id: String, user: User) {

		val page = id.toInt()

		if (interactionType == "time-zone-command-previous") {
			if (page == 1) {
				buttonInteraction.acknowledge()
			} else {
				buttonInteraction.message.createUpdater()
					.addActionRow(
						if (page - 1 == 1) {
							Button.secondary("time-zone-command-previous:${page - 1}", "←")
						} else {
							Button.primary("time-zone-command-previous:${page - 1}", "←")
						},
						Button.primary("time-zone-command-next:${page - 1}", "→"),
						Button.danger("time-zone-command-done:0", "✕")
					)
					.setContent(getZonePages()[page - 2] + "\n\n*Page **${page - 1}** of **${getZonePages().size}***")
					.applyChanges().get()
				buttonInteraction.acknowledge()
			}
		}

		if (interactionType == "time-zone-command-next") {
			if (page == getZonePages().size) {
				buttonInteraction.acknowledge()
			} else {
				buttonInteraction.message.createUpdater()
					.addActionRow(
						Button.primary("time-zone-command-previous:${page + 1}", "←"),
						if (page + 1 == getZonePages().size) {
							Button.secondary("time-zone-command-next:${page + 1}", "→")
						} else {
							Button.primary("time-zone-command-next:${page + 1}", "→")
						},
						Button.danger("time-zone-command-done:0", "✕")
					)
					.setContent(getZonePages()[page] + "\n\n*Page **${page + 1}** of **${getZonePages().size}***")
					.applyChanges().get()
				buttonInteraction.acknowledge()
			}
		}

		if (interactionType == "time-zone-command-done") {
			buttonInteraction.message.delete().get()
			buttonInteraction.acknowledge()
		}

	}

	private fun getZonePages(): List<String> {

		if (cachedZones == null) {
			cachedZones = ZoneId.getAvailableZoneIds().sorted().chunked(25) { it.joinToString("\n") }
		}

		return cachedZones!!

	}

}
