package com.lunamatthews.raidscheduler.slashcommands

import org.javacord.api.interaction.AutocompleteInteraction

interface AutocompleteListener {
	fun handleAutocompleteInteraction(autocompleteInteraction: AutocompleteInteraction)
}
