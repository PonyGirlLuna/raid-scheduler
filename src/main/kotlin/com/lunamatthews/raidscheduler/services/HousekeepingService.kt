package com.lunamatthews.raidscheduler.services

import com.lunamatthews.discordbotcore.services.Service
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.ScheduledEvent
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.util.*
import kotlin.concurrent.fixedRateTimer

class HousekeepingService(private val eventManager: EventManager,
                          private val scheduledEventDb: JsonDB<ScheduledEvent>) : Service("Housekeeping") {

	private val log: Logger = LogManager.getLogger(HousekeepingService::class.java)

	private val eventLifetime = 1000 * 60 * 60 * 24 * 3 //Three days

	private lateinit var timer: Timer

	override fun start() {
		timer = fixedRateTimer(name, true, 5000, 1000 * 60 * 60 * 24) { runHousekeeping() } //Once every day
	}

	override fun stop() {
		timer.cancel()
	}

	//TODO Remove from db also? Maybe move to archive db?
	private fun runHousekeeping() {

		try {

			log.info("Running Housekeeping")

			val currentTime = System.currentTimeMillis()

			//Events are based on epoch seconds, not milliseconds, so multiply by 1000 here
			scheduledEventDb.getAllValues<ScheduledEvent>().filter { !it.archived && currentTime - it.time * 1000 > eventLifetime }.forEach {
				log.info("Event ${it.uniqueId} (${it.definition.name} at ${it.time}) is 1 week old and will be archived.")
				val server = bot.api.getServerById(it.serverId).get()
				eventManager.archiveEvent(it, server)
			}

		} catch (e: InterruptedException) {
			return
		} catch (e: Exception) {
			log.error(e, e)
		}

	}

}
