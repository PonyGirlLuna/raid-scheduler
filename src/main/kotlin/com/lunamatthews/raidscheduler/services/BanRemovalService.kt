package com.lunamatthews.raidscheduler.services

import com.lunamatthews.discordbotcore.services.Service
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.BannedUserList
import com.lunamatthews.raidscheduler.data.Configuration
import com.lunamatthews.raidscheduler.util.getConfigurationByServerId
import com.lunamatthews.raidscheduler.util.saveBannedUserList
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import java.time.ZonedDateTime
import java.util.*
import kotlin.concurrent.fixedRateTimer

class BanRemovalService(private val bannedUserListDb: JsonDB<BannedUserList>,
                        private val configurationDb: JsonDB<Configuration>) : Service("Ban Remover") {

	private val log: Logger = LogManager.getLogger(BanRemovalService::class.java)

	private lateinit var timer: Timer

	override fun start() {
		timer = fixedRateTimer(name, true, 0, 1000 * 60 * 5) { processBanRemovals() } //5 Minutes
	}

	override fun stop() {
		timer.cancel()
	}

	private fun processBanRemovals() {

		log.debug("Running ban removal service")

		try {

			bannedUserListDb.getAllValues<BannedUserList>().forEach { bannedUserList ->

				val usersToUnban = bannedUserList.bans.filter { it.expiration <= ZonedDateTime.now().toEpochSecond() }

				if (usersToUnban.isNotEmpty()) {

					val modChannel = bot.api.getTextChannelById(getConfigurationByServerId(configurationDb, bannedUserList.server).modChannel!!).get()

					usersToUnban.forEach {
						log.info("Unbanning ${it.user} due to a ban expiration")
						MessageBuilder()
							.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
							.setContent("The event joining restriction for <@${it.user}> has expired and has been automatically removed.")
							.send(modChannel)
					}

					bannedUserList.bans.removeAll(usersToUnban)

					saveBannedUserList(bannedUserListDb, bannedUserList)

				}

			}

		} catch (e: InterruptedException) {
			return
		} catch (e: Exception) {
			log.error(e, e)
		}

	}

}
