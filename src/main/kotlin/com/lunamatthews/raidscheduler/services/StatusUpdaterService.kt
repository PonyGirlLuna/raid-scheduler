package com.lunamatthews.raidscheduler.services

import com.lunamatthews.discordbotcore.services.Service
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.activity.ActivityType
import java.util.*
import kotlin.concurrent.fixedRateTimer
import kotlin.jvm.optionals.getOrNull

class StatusUpdaterService : Service("Status Updater") {

	private val log: Logger = LogManager.getLogger(StatusUpdaterService::class.java)

	private val activities = listOf(
		"Trials of Osiris",
		"Iron Banner",
		"Control",
		"Clash",
		"Survival",
		"Elimination",
		"Rumble",
		"Mayhem",
		"Momentum Control",
		"Showdown",
		"Gambit",
		"Altars of Sorrow",
		"Team Scorched",
		"The Shattered Throne",
		"Pit of Heresy",
		"Prophecy",
		"Grasp of Avarice",
		"Duality",
		"Sever",
		"Nightmare Containment",
		"PsiOps",
		"Expedition",
		"Ketchcrash",
		"Wellspring",
		"Vox Obscura",
		"Dares of Eternity",
		"with Starhorse",
		"blackjack with Zavala",
		"The Arms Dealer",
		"Lake of Shadows",
		"Exodus Crash",
		"The Inverted Spire",
		"The Insight Terminus",
		"Warden of Nothing",
		"The Corrupted",
		"The Scarlet Keep",
		"The Glassway",
		"The Disgraced",
		"The Devil's Lair",
		"Fallen S.A.B.E.R.",
		"Proving Ground",
		"The Lightblade",
		"Birthplace of the Vile",
		"Last Wish",
		"Garden of Salvation",
		"Deep Stone Crypt",
		"Vault of Glass",
		"Vow of the Disciple",
		"King's Fall",
		"Shadowkeep",
		"Beyond Light",
		"The Witch Queen",
		"around in the Tower",
		"with physics enabled cubes",
		"Onslaught",
		"Pantheon",
		"Lightfall",
		"The Final Shape",
		"Terminal Overload",
		"Crota's End",
		"Root of Nightmares",
		"Deep Dives",
		"The Coil",
		"Riven's Lair",
		"Defiant Battlegrounds",
		"Salvage",
		"Hypernet Current",
		"Savathûn's Spire",
		"Altars of Summoning",
		"Seasonal Challenges"
	)

	private lateinit var timer: Timer

	override fun start() {
		timer = fixedRateTimer(name, true, 0, 1000 * 60 * 60) { updateStatus() } //One hour
	}

	override fun stop() {
		timer.cancel()
	}

	@OptIn(ExperimentalStdlibApi::class)
	private fun updateStatus() {

		try {

			val currentActivity = bot.api.activity.getOrNull()?.name ?: "None"
			log.info("Current activity: $currentActivity")

			bot.api.updateActivity(ActivityType.PLAYING, activities.filterNot { it == currentActivity }.random())

		} catch (e: InterruptedException) {
			return
		} catch (e: Exception) {
			log.error(e, e)
		}

	}

}
