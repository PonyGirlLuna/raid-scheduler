package com.lunamatthews.raidscheduler.services

import com.lunamatthews.discordbotcore.DiscordBot
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.BannedUserList
import com.lunamatthews.raidscheduler.data.Configuration
import com.lunamatthews.raidscheduler.data.EventDefinition
import com.lunamatthews.raidscheduler.data.ScheduledEvent
import com.lunamatthews.raidscheduler.exceptions.*
import com.lunamatthews.raidscheduler.slashcommands.ButtonListener
import com.lunamatthews.raidscheduler.util.*
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.MessageFlag
import org.javacord.api.entity.message.component.ActionRow
import org.javacord.api.entity.message.component.Button
import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder
import org.javacord.api.entity.permission.PermissionType
import org.javacord.api.entity.server.Server
import org.javacord.api.entity.user.User
import org.javacord.api.interaction.ButtonInteraction
import org.javacord.api.interaction.SlashCommandInteraction
import org.javacord.api.interaction.callback.InteractionOriginalResponseUpdater
import java.awt.Color
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.ExecutionException
import kotlin.jvm.optionals.getOrNull

class EventManager(
	private val bot: DiscordBot,
	private val eventDefinitions: JsonDB<EventDefinition>,
	private val scheduledEvents: JsonDB<ScheduledEvent>,
	private val bannedUserListDb: JsonDB<BannedUserList>,
	private val configurationDb: JsonDB<Configuration>
) : ButtonListener {

	//TODO Handle missing interactions on, say, restart (if necessary???)
	//TODO Figure out if this be removed entirely. It would be nice if it could be.
	private val responders: MutableMap<String, InteractionOriginalResponseUpdater> = mutableMapOf()

	fun createEvent(slashCommandInteraction: SlashCommandInteraction, server: Server, user: Long, eventName: String, dateTime: ZonedDateTime, userTimeZone: String?, threadName: String?, difficulty: String) {

		val configuration = getConfiguration(server.id)

		val uniqueId = nextID()

		val timeZone = if (userTimeZone == null) {
			ZoneId.of("America/New_York")
		} else {
			ZoneId.of(userTimeZone)
		}

		val currentDateTime = ZonedDateTime.now(timeZone)
		var adjustedDateTime = dateTime

		//If before 6am and is in the past, schedule for tomorrow (so midnight, 1am, etc. will not schedule for that morning, but rather the same night)
		if (dateTime.toLocalDate().isEqual(currentDateTime.toLocalDate()) && dateTime.toLocalTime().isBefore(currentDateTime.toLocalTime()) && dateTime.toLocalTime().isBefore(LocalTime.of(6, 0))) {
			adjustedDateTime = adjustedDateTime.plusDays(1)
		} else if (dateTime.isBefore(currentDateTime.plusDays(1)) && dateTime.isBefore(currentDateTime)) {
			throw InvalidDateException("Date/time is in the past.")
		}

		adjustedDateTime = adjustedDateTime.withZoneSameInstant(ZoneId.of("America/New_York"))

		val eventDefinition = eventDefinitions.getAllValues<EventDefinition>().first { it.name == eventName }

		val epochSecondDateTime = adjustedDateTime.toEpochSecond()

		val prohibitedThreadNameCharacters = "[@#:\"<>/\\\\]"

		val threadNameWarning = if (threadName != null && threadName.contains(Regex(prohibitedThreadNameCharacters))) {
			"\n>> Your thread contains invalid characters (`@#:\\\"<>/`) and they may be removed by Discord when your thread is created. <<"
		} else {
			""
		}

		if (difficulty !in eventDefinition.difficulties && difficulty != eventDefinition.defaultDifficulty) {
			throw InvalidDifficultyException("Difficulty not in definition")
		}

		var channel = slashCommandInteraction.channel.get().id

		if (channel != configuration.secondaryEventChannel) {
			channel = configuration.eventChannel!!
		}

		val scheduledEvent = ScheduledEvent(
			uniqueId,
			eventDefinition,
			threadName ?: "${eventDefinition.name} on ${adjustedDateTime.dayOfWeek.name} at ${adjustedDateTime.format(DateTimeFormatter.ofPattern("KK-mm a"))}",
			epochSecondDateTime,
			user,
			server.id,
			channel,
			difficulty = difficulty
		)

		scheduledEvent.spleebs += user

		val confirmEmote = server.getCustomEmojisByName(configuration.confirmEventCreationEmote).first()
		val cancelEmote = server.getCustomEmojisByName(configuration.cancelEventCreationEmote).first()

		responders[uniqueId] = slashCommandInteraction.createImmediateResponder()
			.setContent("***THIS IS A PREVIEW OF YOUR EVENT; YOU MUST CONFIRM IT***\n**Thread name:** ${scheduledEvent.threadName}$threadNameWarning")
			.setFlags(MessageFlag.EPHEMERAL)
			.addEmbed(buildEmbed(scheduledEvent))
			.addComponents(
				ActionRow.of(
					Button.primary("create-raid-post:$uniqueId", "This looks good!", confirmEmote),
					Button.secondary("cancel-raid-post:$uniqueId", "Wait, no, this isn't right.", cancelEmote)
				)
			)
			.setAllowedMentions(AllowedMentionsBuilder().setMentionUsers(false).build())
			.respond().get()

		scheduledEvents.addValue(scheduledEvent)

	}

	private fun buildEmbed(scheduledEvent: ScheduledEvent): EmbedBuilder {

		val eventDefinition = scheduledEvent.definition

		val fireteamSize = if (eventDefinition.maxParticipants == eventDefinition.minParticipants) {
			"${eventDefinition.minParticipants}"
		} else {
			"${eventDefinition.minParticipants}-${eventDefinition.maxParticipants}"
		}

		val (mainTeam, backupTeam, maybeTeam) = getTeams(scheduledEvent)

		val mainTeamTitle = if (mainTeam.isEmpty()) { "Team:" } else { "Team (${mainTeam.size}):" }
		val backupTeamTitle = if (backupTeam.isEmpty()) { "Backup:" } else { "Backup (${backupTeam.size}):" }
		val maybesTeamTitle = if (maybeTeam.isEmpty()) { "Maybes:" } else { "Maybes (${maybeTeam.size}):" }

		val configuration = getConfigurationByServerId(configurationDb, scheduledEvent.serverId)

		val emoteName = when (scheduledEvent.difficulty) {
			"standard" -> { configuration.difficultyEmoteStandard }
			"advanced" -> { configuration.difficultyEmoteAdvanced }
			"expert" -> { configuration.difficultyEmoteExpert }
			"master" -> { configuration.difficultyEmoteMaster }
			"grandmaster" -> { configuration.difficultyEmoteGrandmaster }
			else -> configuration.difficultyEmoteStandard
		}

		val difficultyEmote = if (emoteName == null) {
			":light_blue_heart:"
		} else {
			bot.api.getServerById(scheduledEvent.serverId).get().getCustomEmojisByName(emoteName).first().mentionTag
		}

		val eventTitle = if (eventDefinition.difficulties.size <= 1) {
			"$difficultyEmote ${eventDefinition.name}"
		} else {
			eventDefinition.name + " — $difficultyEmote *" + scheduledEvent.difficulty.replaceFirstChar { it.uppercaseChar() } + "*"
		}

		return EmbedBuilder()
			.setTitle(eventTitle)
			.addField("When:", "<t:${scheduledEvent.time}:F> *(<t:${scheduledEvent.time}:R>)*")
			.addField("Fireteam size:", fireteamSize)
			.addField(
				mainTeamTitle, if (mainTeam.isNotEmpty()) {
					mainTeam.joinToString(", ") { "<@$it>" + if (it in scheduledEvent.reservations) {"\\*"} else {""} }
				} else {
					"*Nobody*"
				})
			.addField(
				backupTeamTitle, if (backupTeam.isNotEmpty()) {
					backupTeam.joinToString(", ") { "<@$it>" }
				} else {
					"*Nobody*"
				})
			.addField(
				maybesTeamTitle, if (scheduledEvent.maybes.isNotEmpty()) {
					maybeTeam.joinToString(", ") { "<@$it>" }
				} else {
					"*Nobody*"
				})
			.addField("Scheduled by:", "<@${scheduledEvent.schedulerId}>")
			.setFooter("ID: ${scheduledEvent.uniqueId}")
			.setColor(Color.decode(eventDefinition.color)).apply {
				if (eventDefinition.icon != null) {
					setThumbnail(eventDefinition.icon)
				}
			}.apply {
				if (eventDefinition.infoLink != null) {
					setDescription("Info Post: " + eventDefinition.infoLink)
				}
			}

	}

	fun getTeams(scheduledEvent: ScheduledEvent): Array<List<Long>> {

		val effectiveEventMaxSize = scheduledEvent.definition.maxParticipants - scheduledEvent.reservations.size
		val spleebsWithoutReservations = scheduledEvent.spleebs.toMutableList().apply { removeAll(scheduledEvent.reservations) }
		val totalSpleebCount = spleebsWithoutReservations.size + scheduledEvent.reservations.size

		val mainTeam = mutableListOf<Long>()
		val backupTeam = mutableListOf<Long>()
		val maybeTeam = scheduledEvent.maybes

		if (totalSpleebCount > scheduledEvent.definition.maxParticipants) {
			mainTeam += spleebsWithoutReservations.subList(0, effectiveEventMaxSize) + scheduledEvent.reservations
			backupTeam += spleebsWithoutReservations.subList(effectiveEventMaxSize, spleebsWithoutReservations.size)
		} else {
			mainTeam += spleebsWithoutReservations + scheduledEvent.reservations
		}

		return arrayOf(mainTeam, backupTeam, maybeTeam)

	}

	private fun confirmCreatePost(id: String) {

		responders[id]!!.removeAllComponents().setContent("*Raid post created. You may safely dismiss this message*").update().get()

		val scheduledEvent = getScheduledEventById(scheduledEvents, id)

		val server = bot.api.getServerById(scheduledEvent.serverId).get()
		val configuration = getConfigurationByServerId(configurationDb, server.id)

		val addToRaidEmote = server.getCustomEmojisByName(configuration.joinEventEmote).first()
		val addToWaitListEmote = server.getCustomEmojisByName(configuration.maybeJoinEventEmote).first()
		val removeFromRaidEmote = server.getCustomEmojisByName(configuration.removeFromEventEmote).first()

		val channel = server.getTextChannelById(scheduledEvent.channelId).get()

		val thread = MessageBuilder()
			.setContent("<@&${configuration.roleToPing}>")
			.addEmbed(buildEmbed(scheduledEvent))
			.addComponents(
				ActionRow.of(
					Button.primary("add-to-raid:${scheduledEvent.uniqueId}", "I wanna waid!", addToRaidEmote),
					Button.secondary(
						"add-to-maybes:${scheduledEvent.uniqueId}",
						"I can maybe waid.",
						addToWaitListEmote
					),
					Button.danger(
						"remove-from-raid:${scheduledEvent.uniqueId}",
						"Wemove me from da waid.",
						removeFromRaidEmote
					)
				)
			)
			.send(channel).get().createThread(scheduledEvent.threadName, 3 * 24 * 60).get() //TODO Configuration for auto archive duration?

		scheduledEvent.messageId = thread.id
		saveScheduledEvent(scheduledEvents, scheduledEvent)

		thread.addThreadMember(scheduledEvent.schedulerId)

	}

	override fun buttonIds(): List<String> {
		return listOf(
			"create-raid-post",
			"cancel-raid-post",
			"add-to-raid",
			"add-to-maybes",
			"remove-from-raid"
		)
	}

	override fun handleButtonInteraction(buttonInteraction: ButtonInteraction, interactionType: String, id: String, user: User) {

		when (interactionType) {
			"create-raid-post" -> {
				buttonInteraction.acknowledge()
				confirmCreatePost(id)
			}

			"cancel-raid-post" -> {
				buttonInteraction.acknowledge()
				scheduledEvents.deleteValue { it.get("uniqueId").asText() == id }
				responders[id]!!.removeAllComponents().setContent("***Event post cancelled.** You may safely dismiss this message*").update().get()
			}

			"add-to-raid" -> {

				if (isUserBanned(user, buttonInteraction.server.get())) {

					val ban = getBannedUserListByServerId(bannedUserListDb, buttonInteraction.server.get().id).bans.single { it.user == user.id }

					buttonInteraction.createImmediateResponder()
						.setFlags(MessageFlag.EPHEMERAL)
						.setContent("You are forbidden from joining events. You may still join the event as a fill in case a spot opens up (hit maybe).\n" +
								"**Reason:** ${ban.note}\n" +
								"**Expires:** <t:${ban.expiration}:f>")
						.respond()

					return

				} else {
					buttonInteraction.acknowledge()
				}

				var changed = false
				val scheduledEvent = getScheduledEventById(scheduledEvents, id)

				scheduledEvent.maybes.remove(user.id)

				if (!scheduledEvent.spleebs.contains(user.id)) {

					if (scheduledEvent.spleebs.size + scheduledEvent.reservations.size >= scheduledEvent.definition.maxParticipants) {
						buttonInteraction.createImmediateResponder()
							.setFlags(MessageFlag.EPHEMERAL)
							.setContent("As the event is currently full, you have been added to the backup team.")
							.respond()
					}

					addParticipantToThread(user, scheduledEvent, buttonInteraction.server.get())

					scheduledEvent.spleebs += user.id
					changed = true

				}

				if (changed) {
					saveScheduledEvent(scheduledEvents, scheduledEvent)
					updateEventMessage(scheduledEvent)
				}

			}

			"add-to-maybes" -> {

				buttonInteraction.acknowledge()

				var changed = false
				val scheduledEvent = getScheduledEventById(scheduledEvents, id)

				if (!scheduledEvent.maybes.contains(user.id)) {
					scheduledEvent.maybes += user.id
					scheduledEvent.spleebs.remove(user.id)
					scheduledEvent.reservations.remove(user.id)
					addParticipantToThread(user, scheduledEvent, buttonInteraction.server.get())
					changed = true
				}

				if (changed) {
					saveScheduledEvent(scheduledEvents, scheduledEvent)
					updateEventMessage(scheduledEvent)
				}

			}

			"remove-from-raid" -> {

				buttonInteraction.acknowledge()

				val scheduledEvent = getScheduledEventById(scheduledEvents, id)

				var changed = scheduledEvent.reservations.remove(user.id)
				changed = scheduledEvent.spleebs.remove(user.id) || changed
				changed = scheduledEvent.maybes.remove(user.id) || changed

				if (changed) {
					saveScheduledEvent(scheduledEvents, scheduledEvent)
					updateEventMessage(scheduledEvent)
				}

			}
		}

	}

	fun removeUserFromEvent(eventId: String, user: Long, server: Long) {

		val scheduledEvent = getScheduledEventById(scheduledEvents, eventId, server)

		if (scheduledEvent.spleebs.remove(user) || scheduledEvent.maybes.remove(user) || scheduledEvent.reservations.remove(user)) {
			saveScheduledEvent(scheduledEvents, scheduledEvent)
			updateEventMessage(scheduledEvent)
		} else {
			throw UserNotInEventException()
		}

	}

	fun addReservation(eventId: String, user: User, server: Server, commandUser: User) {

		val scheduledEvent = getScheduledEventById(scheduledEvents, eventId, server.id)

		if (isUserBanned(user, server)) {
			throw UserIsBannedException()
		}

		if (scheduledEvent.schedulerId == commandUser.id || server.hasPermission(commandUser, PermissionType.MODERATE_MEMBERS)) {

			if (user.id == scheduledEvent.schedulerId) {
				throw UserIsSchedulerException()
			}

			if (scheduledEvent.reservations.size > scheduledEvent.definition.maxParticipants - 1) {
				throw TooManyParticipantsException()
			} else if (!scheduledEvent.reservations.contains(user.id)) {
				scheduledEvent.reservations += user.id
				saveScheduledEvent(scheduledEvents, scheduledEvent)
				updateEventMessage(scheduledEvent)
				addParticipantToThread(user, scheduledEvent, server)
			}

		} else {
			throw UserDoesNotHavePermissionException()
		}

	}

	fun removeReservation(eventId: String, user: User, server: Server, commandUser: User) {

		val scheduledEvent = getScheduledEventById(scheduledEvents, eventId, server.id)

		if (isUserBanned(user, server)) {
			throw UserIsBannedException()
		}

		if (scheduledEvent.schedulerId == commandUser.id || server.hasPermission(commandUser, PermissionType.MODERATE_MEMBERS)) {
			if (scheduledEvent.reservations.contains(user.id)) {
				scheduledEvent.reservations -= user.id
				saveScheduledEvent(scheduledEvents, scheduledEvent)
				updateEventMessage(scheduledEvent)
			}
		} else {
			throw UserDoesNotHavePermissionException()
		}

	}

	fun renameThread(eventId: String, threadName: String, user: User, server: Server) {

		val scheduledEvent = getScheduledEventById(scheduledEvents, eventId, server.id)

		if (scheduledEvent.schedulerId == user.id || server.hasPermission(user, PermissionType.MODERATE_MEMBERS)) {

			val updater = server.getThreadChannelById(scheduledEvent.messageId!!).get().latestInstance.get().createUpdater().setName(threadName).update()
			updater.join()

		} else {
			throw UserDoesNotHavePermissionException()
		}

	}

	private fun addParticipantToThread(user: User, event: ScheduledEvent, server: Server) {
		server.getThreadChannelById(event.messageId!!).get().addThreadMember(user).get()
	}

	fun cancelEvent(eventId: String, user: User, server: Server) {

		if (!scheduledEventExists(scheduledEvents, eventId, server.id)) {
			throw EventDoesNotExistException()
		}

		val configuration = getConfiguration(server.id)

		val scheduledEvent = getScheduledEventById(scheduledEvents, eventId)

		if (scheduledEvent.schedulerId != user.id && !server.canBanUsers(user)) {
			throw UserDoesNotHavePermissionException()
		} else {

			scheduledEvents.deleteValue { it["uniqueId"].asText() == scheduledEvent.uniqueId }

			server.getThreadChannelById(scheduledEvent.messageId!!).get().delete()
			server.getTextChannelById(scheduledEvent.channelId).get().getMessageById(scheduledEvent.messageId!!).get().delete()

		}

	}

	@OptIn(ExperimentalStdlibApi::class)
	fun archiveEvent(scheduledEvent: ScheduledEvent, server: Server) {

		if (scheduledEvent.messageId != null) {

			val channel = server.getTextChannelById(scheduledEvent.channelId).getOrNull()

			try {

				val message = channel?.getMessageById(scheduledEvent.messageId!!)?.get()
				message?.createUpdater()?.removeAllComponents()?.applyChanges()

			} catch (_: ExecutionException) {
				//We don't care if the message is already gone since we were just going to remove buttons from it
			}

		}

		scheduledEvent.archived = true
		saveScheduledEvent(scheduledEvents, scheduledEvent)

	}

	private fun updateEventMessage(scheduledEvent: ScheduledEvent) {

		val configuration = getConfigurationByServerId(configurationDb, scheduledEvent.serverId)

		val teamMessage = bot.api.getMessageById(
			scheduledEvent.messageId!!,
			bot.api.getTextChannelById(scheduledEvent.channelId).get()
		).get()

		teamMessage.edit("<@&${configuration.roleToPing}>", buildEmbed(scheduledEvent))

	}

	private fun getConfiguration(server: Long): Configuration {

		val configuration = getConfigurationByServerId(configurationDb, server)

		if (configuration.modChannel == null
			|| configuration.roleToPing == null
			|| configuration.eventChannel == null
			|| configuration.confirmEventCreationEmote == null
			|| configuration.cancelEventCreationEmote == null
			|| configuration.maybeJoinEventEmote == null
			|| configuration.removeFromEventEmote == null
			|| configuration.joinEventEmote == null
		) {
			throw IncompleteConfigurationException()
		}

		return configuration

	}

	private fun isUserBanned(user: User, server: Server): Boolean {
		return isUserBanned(user.id, server.id)
	}

	private fun isUserBanned(user: Long, server: Long): Boolean {
		return getBannedUserListByServerId(bannedUserListDb, server).bans.any { it.user == user }
	}

}
