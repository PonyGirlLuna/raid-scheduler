package com.lunamatthews.raidscheduler

import com.lunamatthews.discordbotcore.DiscordBot
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.*
import com.lunamatthews.raidscheduler.migrations.BanListMigrations
import com.lunamatthews.raidscheduler.services.BanRemovalService
import com.lunamatthews.raidscheduler.services.EventManager
import com.lunamatthews.raidscheduler.services.HousekeepingService
import com.lunamatthews.raidscheduler.services.StatusUpdaterService
import com.lunamatthews.raidscheduler.slashcommands.AutocompleteListener
import com.lunamatthews.raidscheduler.slashcommands.ButtonListener
import com.lunamatthews.raidscheduler.slashcommands.botcontrol.AboutCommand
import com.lunamatthews.raidscheduler.slashcommands.botcontrol.HelpCommand
import com.lunamatthews.raidscheduler.slashcommands.botcontrol.InviteCommand
import com.lunamatthews.raidscheduler.slashcommands.configuration.ConfigurationCommand
import com.lunamatthews.raidscheduler.slashcommands.configuration.TimeZonesCommand
import com.lunamatthews.raidscheduler.slashcommands.configuration.UserSettingsCommand
import com.lunamatthews.raidscheduler.slashcommands.events.*
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.intent.Intent
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.mention.AllowedMentionsBuilder

class RaidScheduler : DiscordBot("Raid Scheduler") {

	val log: Logger = LogManager.getLogger(RaidScheduler::class.java)

	private val buttonListeners: MutableList<ButtonListener> = mutableListOf()

	override fun getCurrentCommandVersion(): Int {
		return 7
	}

	override fun onStart() {

		/*api.globalSlashCommands.get().forEach {
			it.deleteGlobal().get()
		}

		api.servers.forEach { server ->
			api.getServerSlashCommands(server).get().forEach {
				it.deleteForServer(server).get()
			}
		}

		return*/

		//Wish list:
		//TODO Make day/month month/day order configurable as a user setting
		//TODO Add YYYY-dd-mm support
		//TODO Restart non-running services without having to restart the bot
		//TODO Setting guardian name in case we want a quick lookup
		//TODO "You can guarantee a position by creating an event, but misuse of the system will result in further revoking of privileges."

		val databasesPath = properties.getPathProperty("workingDirectory").resolve("databases")

		val eventDefinitionDb = JsonDB<EventDefinition>(databasesPath.resolve("event-definitions.json"))
		val scheduledEventDb = JsonDB<ScheduledEvent>(databasesPath.resolve("scheduled-events.json"))
		val configurationDb = JsonDB<Configuration>(databasesPath.resolve("configurations.json"))
		val userSettingsDb = JsonDB<UserSettings>(databasesPath.resolve("user-settings.json"))
		val bannedUserListDb = JsonDB<BannedUserList>(databasesPath.resolve("banned-users.json"))

		addMigrations(
			BanListMigrations(bannedUserListDb, configurationDb)
		)

		val eventManager = EventManager(this, eventDefinitionDb, scheduledEventDb, bannedUserListDb, configurationDb)
		buttonListeners += eventManager

		val timeZonesCommand = TimeZonesCommand()
		buttonListeners += timeZonesCommand

		val scheduleEventCommand = ScheduleEventCommand(eventDefinitionDb, eventManager, userSettingsDb)

		addSlashCommands(
			AboutCommand(),
			InviteCommand(),
			HelpCommand(),
			timeZonesCommand,
			ConfigurationCommand(configurationDb),
			scheduleEventCommand,
			ManageEventsCommand(eventDefinitionDb),
			CommitEventChangesCommand(scheduleEventCommand),
			RemoveFromEventCommand(eventManager, scheduledEventDb, configurationDb),
			CancelEventCommand(eventManager, scheduledEventDb, configurationDb),
			RestrictUserCommand(bannedUserListDb, configurationDb, userSettingsDb),
			UnRestrictUserCommand(bannedUserListDb, configurationDb),
			RestrictionsCommand(bannedUserListDb),
			ReservationCommand(eventManager),
			RenameThreadCommand(eventManager),
			PingCommand(eventManager, scheduledEventDb),
			FillCommand(eventManager, scheduledEventDb),
			UserSettingsCommand(userSettingsDb)
		)

		addServices(
			HousekeepingService(eventManager, scheduledEventDb),
			StatusUpdaterService(),
			BanRemovalService(bannedUserListDb, configurationDb)
		)

		api.addButtonClickListener { buttonClickEvent ->

			val buttonInteraction = buttonClickEvent.buttonInteraction
			val (button, id) = buttonInteraction.customId.split(":")

			buttonListeners.single { button in it.buttonIds() }.handleButtonInteraction(buttonInteraction, button, id, buttonInteraction.user)

		}

		api.addAutocompleteCreateListener { autocompleteCreateEvent ->

			val command = commands.singleOrNull { it is AutocompleteListener && it.name == autocompleteCreateEvent.autocompleteInteraction.commandName }

			if (command != null) {
				(command as AutocompleteListener).handleAutocompleteInteraction(autocompleteCreateEvent.autocompleteInteraction)
			}

		}

		val chicken = properties.getStringProperty("chicken")
		val egg = properties.getStringProperty("egg")

		api.addMessageCreateListener {
			if (it.messageContent.contains(egg)) {
				MessageBuilder()
					.setAllowedMentions(AllowedMentionsBuilder().setMentionEveryoneAndHere(true).build())
					.setContent(chicken)
					.send(it.channel)
			}
		}

	}

	override fun intents(): Array<Intent> {
		return arrayOf(
			Intent.MESSAGE_CONTENT,
			Intent.GUILD_MESSAGES,
			Intent.GUILD_VOICE_STATES
		)
	}

}

fun main() {
	RaidScheduler().start()
}
