package com.lunamatthews.raidscheduler.migrations

import com.lunamatthews.discordbotcore.DiscordBot
import com.lunamatthews.discordbotcore.migrations.Migration
import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.BannedUserList
import com.lunamatthews.raidscheduler.data.BannedUserListv4
import com.lunamatthews.raidscheduler.data.Configuration
import com.lunamatthews.raidscheduler.data.UserBan
import com.lunamatthews.raidscheduler.util.getConfigurationByServerId
import org.javacord.api.entity.message.MessageBuilder
import java.time.ZonedDateTime

class BanListMigrations(private val bannedUserListDb: JsonDB<BannedUserList>,
                        private val configurationDb: JsonDB<Configuration>) : Migration {

	override fun run(bot: DiscordBot, commandVersion: Int) {

		if (commandVersion < 5) {

			val transformDb = JsonDB<BannedUserListv4>(bannedUserListDb.path)
			val newValues = transformDb.getAllValues<BannedUserListv4>().map { serverBanList ->

				val newBannedUserList = serverBanList.bannedUsers.map {
					UserBan(
						0,
						it,
						ZonedDateTime.now().toEpochSecond(),
						ZonedDateTime.now().plusMonths(1).toEpochSecond(),
						"No note provided"
					)
				}.toMutableList()

				BannedUserList(serverBanList.server, newBannedUserList)

			}

			bannedUserListDb.clear()

			newValues.forEach {

				val modChannel = getConfigurationByServerId(configurationDb, it.server).modChannel

				if (modChannel != null) {
					MessageBuilder().setContent("Restricted user list has been migrated and new data can be added to restrictions." +
							"\nA best guess has been made, but it is highly recommended to re-restrict any pre-existing restrictions (`/unrestrict` then `/restrict`) to provide more accurate information. " +
							"\nYou can list existing restrictions with `/restrictions`."
					)
						.send(bot.api.getChannelById(modChannel).get().asTextChannel().get())
				}

				bannedUserListDb.addValue(it)

			}

		}

	}

}
