package com.lunamatthews.raidscheduler.util

import org.apache.commons.codec.binary.Base32
import java.nio.ByteBuffer
import java.util.*

@Synchronized
fun nextID(): String {
	val bytes = ByteBuffer.allocate(Long.SIZE_BYTES).putLong(UUID.randomUUID().leastSignificantBits).array()
	return Base32().encodeToString(bytes).substringBefore('=').substring(0..8).lowercase()
}

fun main() {

	println(nextID())
	println(nextID())
	println(nextID())
	println(nextID())

}
