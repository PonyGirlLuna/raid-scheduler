package com.lunamatthews.raidscheduler.util

import com.lunamatthews.discordbotcore.util.JsonDB
import com.lunamatthews.raidscheduler.data.BannedUserList
import com.lunamatthews.raidscheduler.data.Configuration
import com.lunamatthews.raidscheduler.data.ScheduledEvent
import com.lunamatthews.raidscheduler.data.UserSettings
import com.lunamatthews.raidscheduler.exceptions.EventDoesNotExistException

fun getConfigurationByServerId(configurationDB: JsonDB<Configuration>, server: Long): Configuration {
	val configurations = configurationDB.getAllValues<Configuration>()
	if (configurations.none { it.serverId == server }) {
		configurationDB.addValue(Configuration(server, null, null, null, null, null, null, null, null, null, null, null, null, null, null))
	}
	return configurationDB.getAllValues<Configuration>().first { it.serverId == server }
}

fun saveConfiguration(configurationDB: JsonDB<Configuration>, configuration: Configuration) {
	configurationDB.updateValue({ it.get("serverId").asLong() == configuration.serverId }, configuration)
}

fun getUserSettingsById(settingsDB: JsonDB<UserSettings>, user: Long): UserSettings {
	val configurations = settingsDB.getAllValues<UserSettings>()
	if (configurations.none { it.user == user }) {
		settingsDB.addValue(UserSettings(user, null, null))
	}
	return settingsDB.getAllValues<UserSettings>().first { it.user == user }
}

fun saveUserSettings(settingsDB: JsonDB<UserSettings>, userSettings: UserSettings) {
	settingsDB.updateValue({ it.get("user").asLong() == userSettings.user }, userSettings)
}

fun getBannedUserListByServerId(bannedUserListDb: JsonDB<BannedUserList>, server: Long): BannedUserList {
	val bannedUserList = bannedUserListDb.getAllValues<BannedUserList>()
	if (bannedUserList.none { it.server == server }) {
		bannedUserListDb.addValue(BannedUserList(server, mutableListOf()))
	}
	return bannedUserListDb.getAllValues<BannedUserList>().first { it.server == server }
}

fun saveBannedUserList(bannedUserListDb: JsonDB<BannedUserList>, bannedUserList: BannedUserList) {
	bannedUserListDb.updateValue({ it.get("server").asLong() == bannedUserList.server }, bannedUserList)
}

fun scheduledEventExists(scheduledActivitiesDb: JsonDB<ScheduledEvent>, id: String, server: Long): Boolean {
	return scheduledActivitiesDb.getAllValues<ScheduledEvent>().any { it.uniqueId == id && it.serverId == server }
}

fun getScheduledEventById(scheduledActivitiesDb: JsonDB<ScheduledEvent>, id: String): ScheduledEvent {
	return scheduledActivitiesDb.getAllValues<ScheduledEvent>().firstOrNull { it.uniqueId == id } ?: throw EventDoesNotExistException()
}

fun getScheduledEventById(scheduledActivitiesDb: JsonDB<ScheduledEvent>, id: String, server: Long): ScheduledEvent {
	return scheduledActivitiesDb.getAllValues<ScheduledEvent>().firstOrNull { it.uniqueId == id && it.serverId == server } ?: throw EventDoesNotExistException()
}

fun getScheduledEventByThreadId(scheduledActivitiesDb: JsonDB<ScheduledEvent>, id: Long, server: Long): ScheduledEvent {
	return scheduledActivitiesDb.getAllValues<ScheduledEvent>().firstOrNull { it.messageId == id && it.serverId == server } ?: throw EventDoesNotExistException()
}

fun saveScheduledEvent(scheduledActivitiesDb: JsonDB<ScheduledEvent>, scheduledEvent: ScheduledEvent) {
	scheduledActivitiesDb.updateValue({ it.get("uniqueId").asText() == scheduledEvent.uniqueId }, scheduledEvent)
}
