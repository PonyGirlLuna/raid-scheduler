package com.lunamatthews.raidscheduler.util

import com.lunamatthews.raidscheduler.exceptions.InvalidDateException
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalAdjusters
import java.util.*

val formatterPartialYear: DateTimeFormatter = DateTimeFormatter.ofPattern("M/d/yy")
val formatterFullYear: DateTimeFormatter = DateTimeFormatter.ofPattern("M/d/yyyy")

val daysOfWeek = mapOf(
	"sunday" to DayOfWeek.SUNDAY,
	"monday" to DayOfWeek.MONDAY,
	"tuesday" to DayOfWeek.TUESDAY,
	"wednesday" to DayOfWeek.WEDNESDAY,
	"thursday" to DayOfWeek.THURSDAY,
	"friday" to DayOfWeek.FRIDAY,
	"saturday" to DayOfWeek.SATURDAY,
	"sun" to DayOfWeek.SUNDAY,
	"mon" to DayOfWeek.MONDAY,
	"tue" to DayOfWeek.TUESDAY,
	"wed" to DayOfWeek.WEDNESDAY,
	"thu" to DayOfWeek.THURSDAY,
	"fri" to DayOfWeek.FRIDAY,
	"sat" to DayOfWeek.SATURDAY
)

fun main() {

	println(parseDateTime("today", "midnight", "America/Los_Angeles"))
	println(parseDateTime("today", "midnight", null))
	println(parseDateTime("today", "now", null))
	println(parseDateTime("today", "5 minutes", null))
	println(parseDateTime("today", "5 minutes", "America/Los_Angeles"))
	println(parseDateTime("today", "8", "America/Los_Angeles"))
	println(parseDateTime("today", "8", "America/New_York"))
	println(parseDateTime("today", "2am", "America/New_York"))
	println(parseDateTime("9/1/23", "2am", "America/New_York"))
	println(parseDateTime("09/01/23", "2am", "America/New_York"))
	println(parseDateTime("a week", "2am", "America/New_York"))
	println(parseDateTime("in a week", "2am", "America/New_York"))
	println(parseDateTime("1 week", "2am", "America/New_York"))
	println(parseDateTime("2 weeks", "2am", "America/New_York"))
	println(parseDateTime("1 month", "2am", "America/New_York"))

}

fun parseDateTime(date: String, time: String, timeZoneString: String?): ZonedDateTime {

	val timeZone = if (timeZoneString != null) {
		ZoneId.of(timeZoneString)
	} else {
		ZoneId.of("America/New_York")
	}

	val dateTime = try {
		parseTime(time, parseDate(date, timeZone))
	} catch (e: DateTimeParseException) {
		parseTime(date, parseDate(time, timeZone)) //If the command was entered backwards, try it the other way around
	}

	if (dateTime.isBefore(ZonedDateTime.now().withZoneSameInstant(timeZone))) {
		throw InvalidDateException("Date is in the past! Can only do that in the Vault of Glass.")
	}

	return dateTime

}

fun parseDate(input: String, timeZone: ZoneId): ZonedDateTime {

	var normalizedInput = input.lowercase().removePrefix("in").trim()
	val currentDateTime = ZonedDateTime.now().withZoneSameInstant(timeZone)

	return when (normalizedInput) {
		"today", "tonight" -> {
			currentDateTime
		}
		"tomorrow" -> {
			currentDateTime.plusDays(1)
		}
		in daysOfWeek.keys -> {
			currentDateTime.with(TemporalAdjusters.next(daysOfWeek[normalizedInput]))
		}
		else -> {

			when {

				Regex("((a|\\d+)?\\s*(?:day|week|month|year)s?)").matches(normalizedInput) -> {

					normalizedInput = normalizedInput.removeSuffix("s")

					if (normalizedInput.matches(Regex("^a\\s+.*"))) {
						normalizedInput = normalizedInput.replaceFirst("a", "1")
					}

					var output = ZonedDateTime.now(ZoneId.of("America/New_York"))
					val number = normalizedInput.split(Regex("\\s+"))[0].toLong()

					if (normalizedInput.endsWith("day")) {
						output = output.plusDays(number)
					} else if (normalizedInput.endsWith("week")) {
						output = output.plusWeeks(number)
					} else if (normalizedInput.endsWith("month")) {
						output = output.plusMonths(number)
					} else if (normalizedInput.endsWith("year")) {
						output = output.plusYears(number)
					}

					return output.truncatedTo(ChronoUnit.MINUTES)

				}

				else -> {

					val parsedDate = try {
						LocalDate.parse(normalizedInput, formatterPartialYear.withZone(timeZone)).atStartOfDay(timeZone)
					} catch (e: Exception) {
						LocalDate.parse(normalizedInput, formatterFullYear.withZone(timeZone)).atStartOfDay(timeZone)
					}

					parsedDate

				}

			}

		}

	}

}

fun parseTime(time: String, currentTime: ZonedDateTime): ZonedDateTime {

	val formattedTime = time.lowercase(Locale.getDefault()).removePrefix("in").removePrefix("at").trim()

	when (formattedTime) {
		"midnight" -> return currentTime.plusDays(1).truncatedTo(ChronoUnit.DAYS)
		"noon" -> return currentTime.withHour(12).withMinute(0)
		"now" -> return currentTime.plusMinutes(5)
		else -> {}
	}

	val twelveHourPeriod = if (formattedTime.endsWith("am")) {
		"am"
	} else {
		"pm"
	}

	return if (formattedTime.matches(Regex(".*(minutes*|hours*|mins*|[^ap]m)"))) {

		val numberOfTimeUnits = formattedTime.filter { it.isDigit() }.toLong()

		if (formattedTime.matches(Regex(".*(minutes*|mins*|m)"))) {
			currentTime.plusMinutes(numberOfTimeUnits)
		} else {
			currentTime.plusHours(numberOfTimeUnits)
		}

	} else {

		val timeJustNumbers = formattedTime.filter { it.isDigit() || it == ':' }

		var hour = timeJustNumbers.split(":").first().toInt()

		if (hour < 12 && twelveHourPeriod == "pm") {
			hour += 12
		}

		if (hour == 24) {
			hour = 0
		}

		val minute = if (timeJustNumbers.contains(":")) {
			timeJustNumbers.split(":")[1].toInt()
		} else {
			0
		}

		currentTime.withHour(hour).withMinute(minute)

	}.withSecond(0).withNano(0)

}
