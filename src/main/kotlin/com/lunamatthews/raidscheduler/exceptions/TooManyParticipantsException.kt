package com.lunamatthews.raidscheduler.exceptions

class TooManyParticipantsException : Exception()
