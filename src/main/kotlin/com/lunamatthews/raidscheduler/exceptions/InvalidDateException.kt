package com.lunamatthews.raidscheduler.exceptions

class InvalidDateException(message: String) : Exception(message)
