package com.lunamatthews.raidscheduler.exceptions

class UserNotInEventException : Exception()
