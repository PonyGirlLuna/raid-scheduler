package com.lunamatthews.raidscheduler.exceptions

class EventDoesNotExistException : Exception()
