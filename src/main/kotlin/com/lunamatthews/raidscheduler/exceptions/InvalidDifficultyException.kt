package com.lunamatthews.raidscheduler.exceptions

class InvalidDifficultyException(message: String) : Exception(message)
