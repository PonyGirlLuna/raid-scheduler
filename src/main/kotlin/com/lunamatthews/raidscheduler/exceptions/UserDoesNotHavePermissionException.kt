package com.lunamatthews.raidscheduler.exceptions

class UserDoesNotHavePermissionException : Exception()
